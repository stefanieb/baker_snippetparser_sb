package runner;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import compare.APIElement;
import compare.JSONComparator;
import extractor.StackToBaker;
import extractor.StackOverflowPost;
import restAPIAccess.JavaBaker;

public class BakerRunner {

	private static int FROM = 900000;
	private static int TO = 2500;
	private static String FILENAME = "/Users/stefanie/Documents/workspace_baker/JavaSnippetParser_sb/data/code.txt";
	private int postCount;
	private int skipCount;
	private final static String EMPTY_JSON = "{\"api_elements\": [{ \"precision\": \"\",\"name\": \"\",\"line_number\": \"\",\"type\": \"\",\"elements\": \"\"}]}";
	private DBHandler dbh;
	private static String dbUrl = "jdbc:mysql://localhost:3306/";
	private static String dbName = "SO_Nov2014";
	private static String dbUsername = "root";
	private static String dbPassword = "";
	private static String sshUser = "";
	private static String sshPassword = "";
	private static String rHost = "";

	public static void main(String args[]) {
		Properties prop = new Properties();
		InputStream input = null;
		String fname = "config.properties";

		try {
			// load a properties file
			input = new FileInputStream(fname);
			prop.load(input);

			// FROM = Integer.parseInt(prop.getProperty("from"));
			// TO = Integer.parseInt(prop.getProperty("to"));
			FILENAME = prop.getProperty("filename");
			dbUrl = prop.getProperty("dbUrl");
			dbName = prop.getProperty("dbName");
			dbUsername = prop.getProperty("dbUsername");
			dbPassword = prop.getProperty("dbPassword", "");
			sshUser = prop.getProperty("sshUser", "");
			sshPassword = prop.getProperty("sshPassword", "");
			rHost = prop.getProperty("rHost", "");

			System.out.println("======== CONFIG ===========");
			System.out.println("URL: " + dbUrl);
			System.out.println("DB-NAME: " + dbName);
			System.out.println("USER: " + dbUsername);
			System.out.println("PW: " + dbPassword);
			System.out.println("code.txt: " + FILENAME);
			System.out.println("RANGE: " + FROM + " - " + (FROM + TO));
			System.out.println("HOST:" + rHost);
			System.out.println("===========================");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		new BakerRunner();

	}

	public BakerRunner() {

		dbh = new DBHandler(dbUrl, dbName, dbUsername, dbPassword, sshUser, sshPassword, rHost);

		// dbh.dbTest();
		// dbh.dbTestSSH();

		List<StackOverflowPost> posts = dbh.getPostsFromDb(FROM, TO);
		// posts = dbh.getPostFromDb(906850);

		List<StackOverflowPost> skipped = iteratePosts(posts, true);
		// iteratePosts(skipped, false);

		System.out.println("======== SUMMARY ==========");
		System.out.println("noOfPosts: " + posts.size());

		System.out.println("postCount:" + postCount);
		System.out.println("skipCount:" + skipCount);
		System.out.println("===========================");

	}

	private List<StackOverflowPost> iteratePosts(List<StackOverflowPost> posts, boolean first) {
		List<StackOverflowPost> skipped = new ArrayList<>();
		StackToBaker sotb = new StackToBaker();
		int p = 0; // counter for posts
		for (StackOverflowPost post : posts) {
			p++;
			if (p % 1000 == 0) {
				String output = first ? "Runde1: " + p : "Runde2: " + p;
				System.out.println(output);
			}
			Integer postId = post.getId();

			String body = post.getBody();
			String codeSnippets = sotb.extractCodeFromPost(body);
			boolean found = false;
			if (!codeSnippets.equals("") && codeSnippets != null) {
				
					saveIntoCodeFile(codeSnippets);

					String json;
					if (!(json = getJSONfromBaker(FILENAME, postId)).equals("")) {
						found = true;

						parseAndSaveData(json, postId, 1);

					}
				}
		
			if (found) {
				postCount++;
			} else {
				skipCount++;
				skipped.add(post);
			}

		}
		return skipped;
	}

	private void parseAndSaveData(String json, Integer postId, int snippetCount) {
		Object obj = JSONValue.parse(json);

		JSONObject jsonObject = (JSONObject) obj;

		JSONComparator comp = new JSONComparator();
		List<APIElement> apiElements = comp.convertJSONObjectToAPIElementList(jsonObject);

		dbh.saveIntoDB(postId, apiElements, snippetCount);
	}

	private String getJSONfromBaker(String path, Integer postId) {
		String json = "";
		try {
			json = JavaBaker.startBakerExternFile(path);
			json = json.replaceAll("<br>", "");
		} catch (IllegalArgumentException e) {
			System.out.println("catched IllegalArgumentException --> SKIP " + postId);
			// e.printStackTrace();
			json = "";
		} catch (NullPointerException e) {
			System.out.println("catched NullPointerException --> SKIP " + postId);
			// e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}

		// skip if emptyJSOn
		String jsonTest = json.replaceAll("\"", "-").replaceAll("\n", "n");

		if (!json.equals(EMPTY_JSON) && !json.equals("") && !jsonTest.equals("{n   -api_elements-: [n]}")) {

			return json;
		}
		return json;

	}

	private String saveIntoCodeFile(String code) {
		FileWriter fw;
		try {
			fw = new FileWriter(FILENAME, false);

			fw.write(code);
			fw.close();

		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return FILENAME;

	}

}