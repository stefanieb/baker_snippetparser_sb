package runner;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import compare.APIElement;
import extractor.StackOverflowPost;

public class DBHandler {

	private static Charset ENCODING = StandardCharsets.UTF_8;
	private String url = "jdbc:mysql://localhost:3306/";
	private String dbName = "SO_Nov2014";
	private String driver = "com.mysql.jdbc.Driver";
	private String userName = "root";
	private String password = "";
	private String sshUser = "";
	private String sshPassword = "";
	private String rHost = "";
	private int lport = 7473;
	private int rport = 3306;
	private boolean SSH = false;

	public DBHandler(String url, String dbName, String username, String password, String sshUser2, String sshPassword2,
			String rHost2) {
		this.url = url;
		this.dbName = dbName;
		this.userName = username;
		this.password = password;
		this.sshPassword = sshPassword2;
		this.sshUser = sshUser2;
		this.rHost = rHost2;
		this.SSH = true;
	}

	public DBHandler(String url, String dbName, String username, String password) {
		this.url = url;
		this.dbName = dbName;
		this.userName = username;
		this.password = password;
	}

	private void initSSH() {

	}

	private void closeSSH() {

	}

	public List<StackOverflowPost> getPostsFromDb(int from, int to) {

		List<StackOverflowPost> postList = new ArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();
			String query = "SELECT Id,Title,Body,Tags, PostTypeId FROM aposts WHERE Body like '%<code>%</code>%' order by Id LIMIT "
					+ from + ", " + to + ";";
			ResultSet posts = st.executeQuery(query);

			HashMap<Integer, List<String>> returnList = new HashMap<>();

			ArrayList<String> data = null;
			while (posts.next()) {
				data = new ArrayList<>();

				StackOverflowPost post = new StackOverflowPost(posts.getInt("Id"), posts.getString("Body"), 
						posts.getString("Tags"), posts.getString("Title"), posts.getInt("PostTypeId"), posts.getString("CreationDate"), posts.getInt("AcceptedAnswerId"));

				data.add(posts.getString("Title"));
				data.add(posts.getString("Body"));
				data.add(posts.getString("Tags"));

				returnList.put(posts.getInt("Id"), data);
				postList.add(post);
			}

			conn.close();

			return postList;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	public List<StackOverflowPost> getPostFromDb(int id) {

		List<StackOverflowPost> postList = new ArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();
			String query = "SELECT Id,Title,Body,Tags, PostTypeId FROM aposts WHERE Body like '%<code>%</code>%' AND Id = "
					+ id + " order by Id; ";
			ResultSet posts = st.executeQuery(query);

			HashMap<Integer, List<String>> returnList = new HashMap<>();

			ArrayList<String> data = null;
			while (posts.next()) {
				data = new ArrayList<>();

				StackOverflowPost post = new StackOverflowPost(posts.getInt("Id"), posts.getString("Body"), 
						posts.getString("Tags"), posts.getString("Title"), posts.getInt("PostTypeId"), posts.getString("CreationDate"), posts.getInt("AcceptedAnswerId"));

				data.add(posts.getString("Title"));
				data.add(posts.getString("Body"));
				data.add(posts.getString("Tags"));

				returnList.put(posts.getInt("Id"), data);
				postList.add(post);
			}

			conn.close();

			return postList;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	public void saveIntoDB(Integer postId, List<APIElement> apiElements, int snippetCount) {

		for (APIElement elem : apiElements) {
			List<String> classes = elem.getElements();
			for (String cl : classes) {

				Integer elemId = getIdOfElementByFqnTypeAndVersion(cl, elem.getType(), elem.getVersion());
				if (elemId == -1) {
					elemId = saveElement(elem, cl);
				}

				storeElement2Posts(elem, elemId, postId, snippetCount);
			}
		}

	}

	public int saveElement(APIElement elem, String cl) {

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);

			// insert new element
			PreparedStatement updateDataStatement = conn
					.prepareStatement("insert into elements (type, fqn, version) values ('" + elem.getType() + "', '"
							+ cl + "', " + elem.getVersion() + ");");
			updateDataStatement.executeUpdate();
			conn.close();

			return getIdOfElementByFqnTypeAndVersion(cl, elem.getType(), elem.getVersion());

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}

	private void storeElement2Posts(APIElement elem, Integer elemId, Integer postId, int snippetCount) {
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);

			String values = postId + ", " + elem.getPrecision() + ", '" + elem.getName() + "', '" + snippetCount + "_"
					+ elem.getLine_number() + "', " + elemId + ", " + elem.getCharacter();

			PreparedStatement updateDataStatement = conn.prepareStatement(
					"insert into elements2posts (post_id, _precision, name, line, element_id, _char) values (" + values
							+ ");");
			updateDataStatement.executeUpdate();
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MySQLIntegrityConstraintViolationException e) {
			if (e.getMessage().startsWith("Duplicate entry")) {
				System.out.println("duplicate entry: " + postId + ", " + elemId + ", " + elem.getCharacter() + ", "
						+ elem.getLine_number() + ", " + elem.getName());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private int getIdOfElementByFqnTypeAndVersion(String fqn, String type, Double version) {
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			// if element exists, id is returned, otherwise 0
			int elementId = -1;
			ResultSet result = st.executeQuery("select id from elements where elements.fqn='" + fqn
					+ "' and elements.version=" + version + " and type = '" + type + "';");
			if (result.next()) {
				elementId = result.getInt("id");
			}
			conn.close();
			return elementId;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;

	}

	public void dbTest() {
		int count = 0;
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			// if element exists, id is returned, otherwise 0
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			System.out.println("Database connection established");
			System.out.println("DONE");

			ResultSet result = st.executeQuery("select * from aposts limit 0, 1000;");
			while (result.next()) {
				count++;
			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("COUNT " + count);

	}

	public void dbTestSSH() {
		int count = 0;
		Connection conn = null;
		Session session = null;
		try {

			// Class.forName(driver).newInstance();
			// Connection conn = DriverManager.getConnection(url + dbName,
			// userName, password);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();
			session = jsch.getSession(sshUser, rHost, 22);
			session.setPassword(sshPassword);
			session.setConfig(config);
			session.connect();
			String boundaddress = "0.0.0.0";
			int assinged_port = session.setPortForwardingL(boundaddress, lport, rHost, rport);
			System.out.println("Connected");
			// int assinged_port = session.setPortForwardingL(lport, rHost,
			// rport);
			System.out.println("localhost:" + assinged_port + " -> " + rHost + ":" + rport);

			// mysql database connectivity
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url + dbName, userName, password);
			System.out.println("Database connection established");
			System.out.println("DONE");

			Statement st = conn.createStatement();
			ResultSet result = st.executeQuery("select * from aposts limit 0, 1000;");
			while (result.next()) {
				count++;
			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (session != null && session.isConnected()) {
				System.out.println("Closing SSH Connection");
				session.disconnect();
			}
		}
		System.out.println(count);

	}

}
