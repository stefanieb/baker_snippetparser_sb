package worker;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class startPoint {
	
	private static boolean readIdsFromCSV = false;
	private static boolean showOutput = true;
	
	
    private static final String url = "jdbc:mysql://localhost:3306/";
    private static final String dbName = "so2016_12";
    private static final String userName = "root";
    private static final String password = "";
    private static final String driver = "com.mysql.jdbc.Driver";

	public static void main(String[] args) {
		//144254
		
		if(readIdsFromCSV) {
			
			// ----------------------------------------------------
			for(int i = 1; i <= 2; i++) {
			// ----------------------------------------------------
				
				try {
					Class.forName(driver).newInstance();
				
					Connection conn = DriverManager.getConnection(url + dbName, userName, password);
					Statement st = conn.createStatement();
					
					st.executeUpdate("TRUNCATE post2api");
				
				
				
				
				
				BufferedReader br = null;
		        String line = "";
		        
		        try {
		
		            br = new BufferedReader(new FileReader("metrics/testing_posts.csv"));
		            
		            int j = 1;
		            
		            while ((line = br.readLine()) != null) {
		            	System.out.println("[Run #" + i + "] Post " + j + "/400 (" + (1f/4f)*j + "%)");
		            	start(Integer.parseInt(line));
		            	
		            	j++;
		            	
		            	try {
							TimeUnit.MILLISECONDS.sleep(500);// temp fix for incosistency (?)
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		            	
		            	System.out.println("-------------------------------------------------------------------------");
		            }
		
		        } catch (FileNotFoundException e) {
		            e.printStackTrace();
		        } catch (IOException e) {
		            e.printStackTrace();
		        } finally {
		            if (br != null) {
		                try {
		                    br.close();
		                } catch (IOException e) {
		                    e.printStackTrace();
		                }
		            }
		        }
				
		        // ----------------------------------------------------
				
				
				
				
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				try {
					Class.forName(driver).newInstance();
				
					Connection conn = DriverManager.getConnection(url + dbName, userName, password);
					Statement st = conn.createStatement();
				
					String filename = (System.getProperty("user.dir") + "\\data\\run_" + i + " .csv").replaceAll("\\\\", "\\\\\\\\");;
					
					st.executeQuery("SELECT * INTO OUTFILE \"" + filename + "\" FROM post2api");
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				// ----------------------------------------------------
			
			// ----------------------------------------------------	
			}
			// ----------------------------------------------------
		} else {
			//look at posts:
			// 8059719 -> keywords in accepted answer!
			// 5161951 -> baker results are fishy
			int postId = 11804845;
			
//			int numIterations = 1000;
//			int i = 0;
//			
//			int[] vals = new int[5];
//			
//			vals[0] = 9572795; 
//			vals[1] = 9671546; 
//			vals[2] = 10407159; 
//			vals[3] = 12128331; 
//			vals[4] = 15852122;
//			
//			while(i < numIterations) {
//				
//				int pid = vals[new Random().nextInt(vals.length)];
				
				start(postId);
//				i++;
//				try {
//					TimeUnit.MILLISECONDS.sleep(500);// temp fix for incosistency (?)
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
			
		}
		
		
		
		
		// store post
		
	}

	public static void start(int postId) {
		
		if(showOutput)
			System.out.println("processing post #" + postId);
    	
		PostLoader pl = new PostLoader(url, dbName, userName, password, driver);
		
		
		long startTime = System.currentTimeMillis();
		
		StackOverflowPost post = pl.loadPost(postId);
		
		if(showOutput)
			System.out.println("loading post took " + (System.currentTimeMillis() - startTime) + "ms");
		
		if(showOutput)
			System.out.println("min: " + post.getMinVersion() + " | max: " + post.getMaxVersion() + " || found in: " + post.getFoundMinIn() + "/" + post.getFoundMaxIn());
		
		// "dirty" hack to check, if post was already analyzed and thus not loaded by the postloader (postID = -1)
		if(post.getPostID() == postId) {
		
			startTime = System.currentTimeMillis();
			
    		new APIDetector(post, 0);

    		if(showOutput)
    			System.out.println("api detection took " + (System.currentTimeMillis() - startTime) + "ms");

    		if(showOutput)
    			System.out.println("min: " + post.getMinVersion() + " | max: " + post.getMaxVersion() + " || found in: " + post.getFoundMinIn() + "/" + post.getFoundMaxIn());
    		
    		startTime = System.currentTimeMillis();
			
    		try {
				new BakerQuery(post);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		if(showOutput)
    			System.out.println("baker query took " + (System.currentTimeMillis() - startTime) + "ms");
    		
    		if(showOutput)
    			System.out.println("min: " + post.getMinVersion() + " | max: " + post.getMaxVersion() + " || found in: " + post.getFoundMinIn() + "/" + post.getFoundMaxIn());
    		
    		startTime = System.currentTimeMillis();
			
    		// get the accepted answer
    		if(post.getMinVersion() != post.getMaxVersion() && post.getAcceptedAnswerID() > 0) {
        		
    			startTime = System.currentTimeMillis();
    			
    			new APIDetector(post, 1);

        		if(showOutput)
        			System.out.println("> api detection for accepted answer took " + (System.currentTimeMillis() - startTime) + "ms");
        		
        		if(showOutput)
        			System.out.println("min: " + post.getMinVersion() + " | max: " + post.getMaxVersion() + " || found in: " + post.getFoundMinIn() + "/" + post.getFoundMaxIn());
        		
        		// still no distinct version found yet
        		if(post.getMinVersion() != post.getMaxVersion()) {
        		
	    			startTime = System.currentTimeMillis();
	    			
	    			try {
						new BakerQuery(post);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    			
	    			if(showOutput)
	    				System.out.println("> baker query for accepted answer took " + (System.currentTimeMillis() - startTime) + "ms");
	    			
	    			if(post.getMinVersion() > post.getMaxVersion()) {
	    				System.err.println("\n\n\n --- ERROR " + postId + " --- \n\n\n");
	    			}
	    			
	    			if(showOutput)
	        			System.out.println("min: " + post.getMinVersion() + " | max: " + post.getMaxVersion() + " || found in: " + post.getFoundMinIn() + "/" + post.getFoundMaxIn());
	    			
        		}
        		
    		}
    		
    		
    		DBStore db = new DBStore(url, dbName, userName, password, driver);
    		
//    		db.storePost(post);
    		
    		if(showOutput)
    			System.out.println("db storing took " + (System.currentTimeMillis() - startTime));
		
    		if(showOutput)
    			System.out.println("min: " + post.getMinVersion() + " | max: " + post.getMaxVersion() + " || found in: " + post.getFoundMinIn() + "/" + post.getFoundMaxIn() + "\n\n");
    		
		} else {
//			System.out.println("Post already processed -- SKIP");
		}
	}
	
}
