package worker;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;

import worker.StackOverflowPost;

public class APIDetector {
	
	// --- config for the CSV-loading ---
	private static String keywordsCSV = "metrics\\keywords2api.csv";
	private static String releaseDatesCSV = "metrics\\releasedates2api.csv";
	private static String splitChar = ";";
	
	private static HashMap<String, ArrayList<Integer>> keywordsToVersionMap;
	private static SortedMap<Integer, Integer> versionReleaseDatesList;
	
	private int totalMinVersion;	  // total minimum API-Version
	private int totalMaxVersion;	  // total maximum API-Version
	
	// enumeration for the found-status
	// found api-level in:
	//  0...total min or max version
	//	1...post code (baker)
	//	2...post title
	//	3...post body
	//	4...post tags
	//	5...accepted answer code (baker)
	//	6...accepted answer body
	//	7...datum
	
	private static enum FoundIn {
		TOTAL_MIN_OR_MAX_VERSION(0),
		POST_CODE(1),
		POST_TITLE(2),
		POST_BODY(3),
		POST_TAGS(4),
		ANSWER_CODE(5),
		ANSWER_BODY(6),
		POST_DATE(7);
		
		private int value;
		
		FoundIn(int val) {
			this.value = val;
		}
	}
	
	private static boolean showOutput = false;

	// post type:
	// 0 ... analyze question
	// 1 ... analyze the accepted answer
	public APIDetector(StackOverflowPost p, int postType) {
		
		// load the keywords <-> versions CSV
		keywordsToVersionMap = new HashMap<String, ArrayList<Integer>>();
		
		BufferedReader br = null;
        String line = "";
        
        try {
        	
        	br = new BufferedReader(new FileReader(keywordsCSV.replace("\\", System.getProperty("file.separator"))));
        	
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] keyValue = line.split(splitChar);

                String keyword = keyValue[0];
                
                ArrayList<Integer> versions = new ArrayList<Integer>();
               
                for(int i = 1; i < keyValue.length; i++) {
                	try {
                    	versions.add(Integer.parseInt(keyValue[i].trim()));
                	} catch(NumberFormatException e) {
                		System.err.println("Your CSV-file is not valid");
                		e.printStackTrace();
                	}
                }
                
                keywordsToVersionMap.put(keyword, versions);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        // load the releaseDate <-> versions CSV
        
        versionReleaseDatesList = new TreeMap<Integer, Integer>();
        
        try {

        	br = new BufferedReader(new FileReader(releaseDatesCSV.replace("\\", System.getProperty("file.separator"))));
        	
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] keyValue = line.split(splitChar);
                try {
                	versionReleaseDatesList.put(Integer.parseInt(keyValue[0].trim()), Integer.parseInt(keyValue[1].trim()));
                } catch(NumberFormatException e) {
            		System.err.println("Your CSV-file is not valid");
            		e.printStackTrace();
            	}
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        if(postType == 0) {
//        	System.out.println(versionReleaseDatesList.firstKey());
//        	System.out.println(versionReleaseDatesList.get(versionReleaseDatesList.firstKey()));
//        	
//        	
//        	System.out.println(versionReleaseDatesList.lastKey());
//        	System.out.println(versionReleaseDatesList.get(versionReleaseDatesList.lastKey()));
        	// load the total minimum and maximum version-numbers
            this.totalMinVersion = versionReleaseDatesList.get(versionReleaseDatesList.firstKey());
//            this.totalMinVersion = 3; // temporary
            this.totalMaxVersion = versionReleaseDatesList.get(versionReleaseDatesList.lastKey());
        	
        	p.setFoundMinIn(FoundIn.TOTAL_MIN_OR_MAX_VERSION.value);
        }
        detectAPI(p, postType);
        
	}

	private void detectAPI(StackOverflowPost p, int postType) {
		
		try {
			
			int min;
			int max;
			
			if(postType == 0) {
				min = this.totalMinVersion;
				max = this.totalMaxVersion;
				
				p.setMinVersion(min);
				p.setMaxVersion(max);
				p.setFoundMinIn(FoundIn.TOTAL_MIN_OR_MAX_VERSION.value);
				
			} else {
				min = p.getMinVersion();
				max = p.getMaxVersion();
			}

			/* versionnumber in...
			 * 
			 * 			 ID
			 * tags:   6749261
			 * body:   2583150
			 * title:  8989622
			 * 
			 */
			
			// --- order of search ---
			// 1. date
			// 2. tags
			// 3. body
			// 4. title
			
			// these steps are only taken for a question, because an answer does not have a title etc.
			
			if(postType == 0) {
			
				// --- 1. check the date of the question ---
				if(p.getCreationDate() != null && !p.getCreationDate().equals("")) {
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					
					Date date = dateFormat.parse(p.getCreationDate().trim());
				    long creationDateUNIX = (long) date.getTime()/1000;
				    
				    boolean found = false;
				    
				    int oldMax = max;
				    		    
				    for(Integer d : versionReleaseDatesList.keySet()) {
				    	if(d.intValue() < creationDateUNIX) {
				    		found = true;
				    		if(versionReleaseDatesList.get(d) < oldMax) {
				    			max = versionReleaseDatesList.get(d);
				    			
				    			p.setFoundMaxIn(FoundIn.POST_DATE.value);
				    		}
				    	}
				    }
				    
				    // TODO: if total minimum is set to 3, what about the posts which are max 2 per date?
				    
				    if(found) {
				    	if(showOutput)
				    		System.out.println("#> Found new [max] in date: " + max);
				    	p.setMaxVersion(max);
				    	p.setFoundMaxIn(FoundIn.POST_DATE.value);
				    } else {
				    	p.setMaxVersion(this.totalMinVersion);
				    	p.setFoundMaxIn(FoundIn.POST_DATE.value);
				    }
				} else {
					p.setMaxVersion(this.totalMaxVersion);
					p.setFoundMaxIn(FoundIn.TOTAL_MIN_OR_MAX_VERSION.value);
				}
				
				if(showOutput)
					System.out.println("#> min/max after date: " + p.getMinVersion() + " - " + p.getMaxVersion());
				
				if(p.getMinVersion() == p.getMaxVersion()) {
					return;
				}
				
				
				HashMap<String, ArrayList<Integer>> copyList = (HashMap<String, ArrayList<Integer>>)keywordsToVersionMap.clone();
			
				// --- 2. search in tags ---
				ArrayList<String> tags = new ArrayList<String>();
				
				String tagStr = p.getTags();
				if(tagStr != null) { //no tags?
					tags.addAll(Arrays.asList(tagStr.substring(1, tagStr.length() -1).split("><")));
				}
				
				
				// TODO: what if there are multiple tags with a distinct version number?
				// if the post is a question and there are tags given
				if(!tags.isEmpty()) {
					for(String s : tags) {
						if(copyList.containsKey(s)){
							
							if(showOutput)
								System.out.println("#> Found match in tag: " + s);
							
							ArrayList<Integer> curElemVersions = copyList.get(s);
							
							// if the keyword links to multiple version-numbers
							if(copyList.get(s).size() > 1){
								if(min > curElemVersions.get(0)){
									min = curElemVersions.get(0);
									
									p.setFoundMinIn(FoundIn.POST_TAGS.value);
									
									if(showOutput)
										System.out.println("#> Found new [min] in tags: " + min);
								}
								if(max < curElemVersions.get(curElemVersions.size()-1)){
									max = curElemVersions.get(curElemVersions.size()-1);
	
									p.setFoundMaxIn(FoundIn.POST_TAGS.value);
									
									if(showOutput)
										System.out.println("#> Found new [max] in tags: " + max);
								}
							} else {
								// keyword links only to one distinct version number
								min = curElemVersions.get(0);
	
								p.setFoundMinIn(FoundIn.POST_TAGS.value);
								
								if(showOutput)
									System.out.println("#> Found new [min] in tags: " + min);
							
								max = curElemVersions.get(0);
	
								p.setFoundMaxIn(FoundIn.POST_TAGS.value);
								
								if(showOutput)
									System.out.println("#> Found new [max] in tags: " + max);
							}
						}
					}
				}
				
				if(showOutput)
					System.out.println("#> min/max after tags: " + min + " - " + max);
				
				if(min == max) {
					p.setMinVersion(min);
					p.setMaxVersion(max);
					return;
				}
				
				
				
				
				// --- 3. search in title of question ---
				for(String s : copyList.keySet()){
						
					String regex = "[.:+>< ]{0,1}("+ s.toLowerCase() + ")[.:+>< ]{0,1}";
					
					Pattern pattern = Pattern.compile(regex);
					
	//					System.out.println(p.getTitle().toLowerCase());
	//					System.out.println(s.toLowerCase());
					
					Matcher m = pattern.matcher(p.getTitle().toLowerCase());
					
					if(m.find()) {
						
						if(showOutput)
							System.out.println("#>> Found match in title: " + s);
						
						ArrayList<Integer> curElemVersions = copyList.get(s);
						if(curElemVersions.size() > 1){
							if(min > curElemVersions.get(0)){
								min = curElemVersions.get(0);
								
								p.setFoundMinIn(FoundIn.POST_TITLE.value);
								
								if(showOutput)
									System.out.println("#> Found new [min] in title: " + min);
							}
							if(max < curElemVersions.get(curElemVersions.size()-1)){
								max = curElemVersions.get(curElemVersions.size()-1);
								
								p.setFoundMaxIn(FoundIn.POST_TITLE.value);
								
								if(showOutput)
									System.out.println("#> Found new [max] in title: " + max);
							}
						} else {
							if(min < curElemVersions.get(0)){
								min = curElemVersions.get(0);
								
								p.setFoundMinIn(FoundIn.POST_TITLE.value);
								
								if(showOutput)
									System.out.println("#> Found new [min] in title: " + min);
							}
							if(max > curElemVersions.get(0)){
								max = curElemVersions.get(0);
								
								p.setFoundMaxIn(FoundIn.POST_TITLE.value);
								
								if(showOutput)
									System.out.println("#> Found new [max] in title: " + max);
							}
						}
					}
				}
		
				if(showOutput)
					System.out.println("#> min/max after title: " + min + " - " + max);
				
				if(min == max) {
					p.setMinVersion(min);
					p.setMaxVersion(max);
					return;
				}
			}
			
			// --- 4. search in body ---
			
			//delete the code inside the question
			String body;
			
			if(postType == 0) {
				body = p.getBody();
			} else {
				if(p.getAcceptedAnswerBody() == null) {
					body = "";
				} else {
					body = p.getAcceptedAnswerBody();
				}
			}
			
			body = body.replaceAll("(<code>).*?(</code>)", " ");
			
			//delete all tags
			body = body.replaceAll("<[a-zA-Z][a-zA-Z0-9= \":/.-]*>", " "); //tag-beginning
			body = body.replaceAll("(</[a-zA-Z]+>)", " "); //tag-end
			
			//delete special chars (line-break etc)
			body = body.replaceAll("(&#[a-zA-Z]+;)", " ");
			
			//delete multiple whitespaces and toLowerCase
			body = body.replaceAll("([ ])+", " ").toLowerCase();
		
			
			// temporary values for the analyzation of the accepted answer
			ArrayList<Integer> foundVersions = new ArrayList<Integer>();
			
			
			HashMap<String, ArrayList<Integer>> copyList = (HashMap<String, ArrayList<Integer>>)keywordsToVersionMap.clone();
			
			for(String s : copyList.keySet()){
				
				String regex = "\b[.:+>< ]{0,1}("+ s.toLowerCase() + ")[.:+>< ]{0,1}\b";
				
				Pattern pattern = Pattern.compile(regex);
				
				Matcher m = pattern.matcher(body);
				
				if(m.find()) {
					
					if(showOutput)
						System.out.println("#>> Found match in body: " + s);
					
					ArrayList<Integer> curElemVersions = copyList.get(s);
					if(curElemVersions.size() > 1){
						
						if(postType == 1) {
							if(p.getMinVersion() < curElemVersions.get(0)){
								if(!foundVersions.contains(curElemVersions.get(0))) {
									foundVersions.add(curElemVersions.get(0));
								}
							}
							if(p.getMaxVersion() > curElemVersions.get(curElemVersions.size()-1)){
								if(!foundVersions.contains(curElemVersions.get(curElemVersions.size()-1))) {
									foundVersions.add(curElemVersions.get(curElemVersions.size()-1));
								}
							}
						} else {
							if(min < curElemVersions.get(0)){
								if(!foundVersions.contains(curElemVersions.get(0))) {
									foundVersions.add(curElemVersions.get(0));
								}
								p.setFoundMinIn(FoundIn.POST_BODY.value);
							}
							if(max > curElemVersions.get(curElemVersions.size()-1)){
								if(!foundVersions.contains(curElemVersions.get(curElemVersions.size()-1))) {
									foundVersions.add(curElemVersions.get(curElemVersions.size()-1));
								}
								p.setFoundMaxIn(FoundIn.POST_BODY.value);
							}
						}
					} else {
						if(postType == 1) {
							if(p.getMinVersion() < curElemVersions.get(0)){
								if(!foundVersions.contains(curElemVersions.get(0))) {
									foundVersions.add(curElemVersions.get(0));
								}
							}
							if(p.getMaxVersion() > curElemVersions.get(0)){
								if(!foundVersions.contains(curElemVersions.get(0))) {
									foundVersions.add(curElemVersions.get(0));
								}
							}
						} else {
							if(min < curElemVersions.get(0)){
								if(!foundVersions.contains(curElemVersions.get(0))) {
									foundVersions.add(curElemVersions.get(0));
								}
								p.setFoundMinIn(FoundIn.POST_BODY.value);
							}
							if(max > curElemVersions.get(curElemVersions.size()-1)){
								if(!foundVersions.contains(curElemVersions.get(curElemVersions.size()-1))) {
									foundVersions.add(curElemVersions.get(curElemVersions.size()-1));
								}
								p.setFoundMaxIn(FoundIn.POST_BODY.value);
							}
						}
					}
				}
			}
			
			// if there was a keyword found
			if(foundVersions.size() > 0) {
				
				int tempMin = foundVersions.get(foundVersions.indexOf(Collections.min(foundVersions)));
				int tempMax = foundVersions.get(foundVersions.indexOf(Collections.max(foundVersions)));
				
				if(p.getMinVersion() < tempMin && tempMin < p.getMaxVersion()) {
					p.setMinVersion(tempMin);
					
					// if the answer was analyzed
					if(postType == 1) {
						p.setFoundMinIn(FoundIn.ANSWER_BODY.value);
					} else {
						p.setFoundMinIn(FoundIn.POST_BODY.value);
					}
				}
				
				if(p.getMaxVersion() > tempMax && tempMax > p.getMinVersion()) {
					p.setMaxVersion(tempMax);
					
					// if the answer was analyzed
					if(postType == 1) {
						p.setFoundMaxIn(FoundIn.ANSWER_BODY.value);
					} else {
						p.setFoundMaxIn(FoundIn.POST_BODY.value);
					}
				}
			}
			
			// reset the ArrayList
			foundVersions = new ArrayList<Integer>();
			
			//matches android x.x or android x.x.x
			Pattern pattern = Pattern.compile("(android [.0-9]{3}([.0-9]{2})?)");
			Matcher m = pattern.matcher(body);
			
			if(m.find()) {
				for (int i = 0; i < m.groupCount(); i++) {
					
					String searchStr = m.group(i);
					
					if(searchStr != null){
						
						if(copyList.containsKey(searchStr)){
							
							if(showOutput)
								System.out.println("#>> Found match in body: " + searchStr);
							
							ArrayList<Integer> curElemVersions = copyList.get(searchStr);
							if(curElemVersions.size() > 1){
								
								if(postType == 1) {
									if(p.getMinVersion() < curElemVersions.get(0)){
										if(!foundVersions.contains(curElemVersions.get(0))) {
											foundVersions.add(curElemVersions.get(0));
										}
									}
									if(p.getMaxVersion() > curElemVersions.get(curElemVersions.size()-1)){
										if(!foundVersions.contains(curElemVersions.get(curElemVersions.size()-1))) {
											foundVersions.add(curElemVersions.get(curElemVersions.size()-1));
										}
									}
								} else {
									if(min < curElemVersions.get(0)){
										if(!foundVersions.contains(curElemVersions.get(0))) {
											foundVersions.add(curElemVersions.get(0));
										}
										p.setFoundMinIn(FoundIn.POST_BODY.value);
									}
									if(max > curElemVersions.get(curElemVersions.size()-1)){
										if(!foundVersions.contains(curElemVersions.get(curElemVersions.size()-1))) {
											foundVersions.add(curElemVersions.get(curElemVersions.size()-1));
										}
										p.setFoundMaxIn(FoundIn.POST_BODY.value);
									}
								}
							} else {
								if(postType == 1) {
									if(p.getMinVersion() < curElemVersions.get(0)){
										if(!foundVersions.contains(curElemVersions.get(0))) {
											foundVersions.add(curElemVersions.get(0));
										}
									}
									if(p.getMaxVersion() > curElemVersions.get(0)){
										if(!foundVersions.contains(curElemVersions.get(0))) {
											foundVersions.add(curElemVersions.get(0));
										}
									}
								} else {
									if(min < curElemVersions.get(0)){
										if(!foundVersions.contains(curElemVersions.get(0))) {
											foundVersions.add(curElemVersions.get(0));
										}
										p.setFoundMinIn(FoundIn.POST_BODY.value);
									}
									if(max > curElemVersions.get(curElemVersions.size()-1)){
										if(!foundVersions.contains(curElemVersions.get(curElemVersions.size()-1))) {
											foundVersions.add(curElemVersions.get(curElemVersions.size()-1));
										}
										p.setFoundMaxIn(FoundIn.POST_BODY.value);
									}
								}
							}
							
							// if there was a keyword found
							if(foundVersions.size() > 0) {
								
								int tempMin = foundVersions.get(foundVersions.indexOf(Collections.min(foundVersions)));
								int tempMax = foundVersions.get(foundVersions.indexOf(Collections.max(foundVersions)));
								
								if(p.getMinVersion() < tempMin && tempMin < p.getMaxVersion()) {
									p.setMinVersion(tempMin);
									
									// if the answer was analyzed
									if(postType == 1) {
										p.setFoundMinIn(FoundIn.ANSWER_BODY.value);
									} else {
										p.setFoundMinIn(FoundIn.POST_BODY.value);
									}
								}
								
								if(p.getMaxVersion() > tempMax && tempMax > p.getMinVersion()) {
									p.setMaxVersion(tempMax);
									
									// if the answer was analyzed
									if(postType == 1) {
										p.setFoundMaxIn(FoundIn.ANSWER_BODY.value);
									} else {
										p.setFoundMaxIn(FoundIn.POST_BODY.value);
									}
								}
							}
							
							
						}
					}
	            }
			}

			
			if(showOutput)
				System.out.println("#> min/max after body: " + p.getMinVersion() + " - " + p.getMaxVersion());
			
//			if(min == max) {
//				p.setMinVersion(min);
//				p.setMaxVersion(max);
//				return;
//			}
//			
//			if(min < p.getMinVersion()) {
//				p.setMinVersion(min);
//			}
//			
//			if(max < p.getMaxVersion()) {
//				p.setMaxVersion(max);
//			}
			
			
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

}
