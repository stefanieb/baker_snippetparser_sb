package worker;

import java.io.Serializable;

public class StackOverflowPost implements Serializable {

	private Integer postID;
	private String body;
	private String tags;
	private String title;
	private Integer type;
	private String creationDate;
	private Integer acceptedAnswerId;
	private String acceptedAnswerBody;
	private int foundMinIn;
	private int foundMaxIn;
	private int minVersion;
	private int maxVersion;
	
	/*
	 * Title:
	 * 1 ... question
	 * 2 ... answer
	 */

	public StackOverflowPost(Integer id, String body, String tags, String title, Integer type, String cdate, Integer acceptedAnswerId, String acceptedAnswerBody) {
		this.postID = id;
		this.body = body;
		this.tags = tags;
		this.title = title;
		this.type = type;
		this.creationDate = cdate;
		this.acceptedAnswerId = acceptedAnswerId;
		this.acceptedAnswerBody = acceptedAnswerBody;
		this.minVersion = Integer.MAX_VALUE;
		this.maxVersion = -1;
		this.foundMinIn = -1;
		this.foundMaxIn = -1;
		
	}

	public Integer getPostID() {
		return postID;
	}

	public String getBody() {
		return body;
	}

	public String getTags() {
		return tags;
	}

	public String getTitle() {
		return title;
	}

	public Integer getType() {
		return type;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public Integer getAcceptedAnswerID() {
		return acceptedAnswerId;
	}
	
	public String getAcceptedAnswerBody() {
		return acceptedAnswerBody;
	}

	public int getMinVersion() {
		return minVersion;
	}

	public void setMinVersion(int minVersion) {
		this.minVersion = minVersion;
	}

	public int getMaxVersion() {
		return maxVersion;
	}

	public void setMaxVersion(int maxVersion) {
		this.maxVersion = maxVersion;
	}

	public int getFoundMinIn() {
		return foundMinIn;
	}

	public void setFoundMinIn(int foundMinIn) {
		this.foundMinIn = foundMinIn;
	}

	public int getFoundMaxIn() {
		return foundMaxIn;
	}

	public void setFoundMaxIn(int foundMaxIn) {
		this.foundMaxIn = foundMaxIn;
	}


}
