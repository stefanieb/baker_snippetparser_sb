package worker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.MediaType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import workerTools.JavaBaker;

public class BakerQuery {

	private String codeSnippet;
	
	private static List<String> fqn = new ArrayList<String>();
	private final static String emptyJSON = "{\"min_version\":\"\",\"max_version\":\"\",\"api_elements\": [{\"min_version\":\"\",\"max_version\":\"\", \"precision\": \"\",\"name\": \"\",\"line_number\": \"\",\"type\": \"\",\"elements\": \"\"}]}";
	
	// enumeration for the found-status
	// found api-level in:
	//	1...post code (baker)
	//	2...post title
	//	3...post body
	//	4...post tags
	//	5...accepted answer code (baker)
	//	6...accepted answer body
	//	7...datum
	
	private static enum FoundIn {
		TOTAL_MIN_OR_MAX_VERSION(0),
		POST_CODE(1),
		POST_TITLE(2),
		POST_BODY(3),
		POST_TAGS(4),
		ANSWER_CODE(5),
		ANSWER_BODY(6),
		POST_DATE(7);
		
		private int value;
		
		FoundIn(int val) {
			this.value = val;
		}
	}
	
	public BakerQuery(StackOverflowPost post) throws Exception {
		
		// only start baker, if there is no distinct version found yet
		if(post.getMinVersion() != post.getMaxVersion()) {
			
//			System.out.println("Baker processed #" + post.getPostID());

			this.codeSnippet = extractCodeFromPost(post.getBody());
//			System.exit(1);
			
			if(!this.codeSnippet.equals("")) {
				String json = "";
				
				// call the bakertool and retreive the JSON with API-Versions
				json = getJSONfromBaker();
				
//				System.out.println("\n\n" + this.codeSnippet + "\n\n");
				
				if(isValidJSON(json)) {
					getFQN(json);
				}
				
				query(post);
			} else {
//				System.err.println("[BakerQuery] empty code-snippet -> skip post");
			}
			
		}
	}
	
	public void query(StackOverflowPost post) throws Exception {
		if(fqn.size() > 0) {
			
			int postTotalMin = post.getMinVersion();
			int postTotalMax = post.getMaxVersion();
			
			for (String f : fqn) {
				
					
//					System.out.println(f);
					
					// TODO: optimize query
					
					String cypher;
					if(f.contains("(")) {
						cypher = "MATCH (n:method { fullName: '"+f.replaceAll("\\[\"|\"\\]", "")+"' }) RETURN collect(distinct n.version) AS `versions`";
					} else {
						cypher = "MATCH (n:param { fullName: '"+f.replaceAll("\\[\"|\"\\]", "")+"' }) RETURN collect(distinct n.version) AS `versions` UNION MATCH (n:class { fullName: '"+f.replaceAll("\\[\"|\"\\]", "")+"' }) RETURN collect(distinct n.version) AS `versions`";
					}
					
					Client client = Client.create();
					
					
					JSONObject json = new JSONObject();
					json.put("query", cypher);
					
					WebResource resource = client.resource("http://localhost:7474/db/data/cypher");
					
					ClientResponse response = resource
												.accept(MediaType.APPLICATION_JSON)
												.type(MediaType.APPLICATION_JSON)
												.entity(json.toString().replaceAll("[0-9]*[.][0-9]*$", ""))
												.post(ClientResponse.class);
					
					String jsonString = response.getEntity(String.class);
					
//					System.out.println(cypher);
					
					response.close();
					resource = null;
					client.destroy();
					
					JSONParser parser = new JSONParser();

					JSONObject jsonObj = (JSONObject) parser.parse(jsonString);
					
					//System.out.println(jsonObj.toString());
					
					// ugly
					// return from DB:
					// [10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0,19.0,20.0,21.0,22.0,23.0,24.0,4.0,5.0,6.0,7.0,8.0,9.0]
					// replace all [ ] and .0 and then split it into x strings to parse
					String[] versions = jsonObj.get("data").toString().replaceAll("\\[|\\]|[.][0-9]*", "").split(",");
					
					
					if(versions.length > 0) {
						int min = Integer.MAX_VALUE;
						int max = -1;
						
						int v = -1;
						
						for(int i = 0; i < versions.length; i++) {
							if(!versions[i].equals("")) {
								
								v = Integer.parseInt(versions[i]);
								if(min > v) {
									min = v;
								}
								
								if(max < v) {
									max = v;
								}
							}
						}
					
						if(min != Integer.MAX_VALUE && min < postTotalMax && min > postTotalMin)
							postTotalMin = min;
						
						if(max > 0 && max < postTotalMax)
							postTotalMax = max;
					}
					
//					System.out.println(postTotalMin + " - " + postTotalMax);
			}
			
			// baker tends to have "hick-ups" if it is under load and returns something like "min: 4 max: 3"
			if(postTotalMin < postTotalMax) {
				if(post.getMinVersion() < postTotalMin) {
					post.setMinVersion(postTotalMin);
					if(post.getType() == 1) {
						post.setFoundMinIn(FoundIn.POST_CODE.value);
					} else {
						post.setFoundMinIn(FoundIn.ANSWER_CODE.value);
					}
				}
				
				
				if(post.getMaxVersion() > postTotalMax) {
					post.setMaxVersion(postTotalMax);
					if(post.getType() == 1) {
						post.setFoundMinIn(FoundIn.POST_CODE.value);
					} else {
						post.setFoundMinIn(FoundIn.ANSWER_CODE.value);
					}
				}
			}
			
		}
	}

	/**
	 * gets the JSON from the baker with all API-versions
	 * @param codeSnippet a code-snippet
	 * @return JSON-String
	 * @throws NullPointerException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	private String getJSONfromBaker() throws Exception {
		
		String json = "";
		
		json = JavaBaker.startBakerExternBody(this.codeSnippet);
		
		json = json.replaceAll("<br>", "");
		
		return json;
		
	}
	
	/**
	 * validates a given JSON
	 * @param json
	 * @return true - if JSON is valid | false - if json is empty/invalid
	 */
	private boolean isValidJSON(String json) {
		if(!json.equals("") && !json.equals(emptyJSON)) {
			return true;
		} else {
			return false;
		}
	}

	private void getFQN(String json) {
		Object obj = JSONValue.parse(json);
		
		
		JSONObject jsonObject = (JSONObject) obj;

		Object o = jsonObject.get("api_elements");
		
//		System.out.println(jsonObject.toJSONString());
		
		// only a single JSONObject in elements-object
		if (o instanceof JSONObject) {
			
			JSONObject oo = (JSONObject) jsonObject.get("api_elements");
			
//			System.out.println(oo.toJSONString());

			// -----------------------------
			fqn.add(oo.get("elements").toString());
			// -----------------------------
			
		} else if (o instanceof JSONArray) {
			
			JSONArray array = (JSONArray) jsonObject.get("api_elements");
				
	
//			System.out.println(array.toJSONString());
				
			// -----------------------------
			for (Iterator<?> iter = array.iterator(); iter.hasNext();) {
				JSONObject oo = (JSONObject) iter.next();
				fqn.add(oo.get("elements").toString());
			}
			// -----------------------------
			
			
		}
		
	}
	
	/**
	 * extracts all codeBlocks from a posts
	 * code blocks are detected with the <code>-tag
	 * @param body  the body of the post
	 * @return a string with all blocks of code
	 */
	public String extractCodeFromPost(String body) {

		String codeOfBody = "";

		Pattern pattern = Pattern.compile("<code>(.+?)</code>");
		Matcher matcher = pattern.matcher(body);
		
		Pattern logcatRegex = Pattern.compile("[A-Z][/][a-zA-Z\\._ -]+[(][0-9 ]+[)]");
		
		Pattern stackTraceRegex = Pattern.compile("(([a-zA-Z]+[\\.]){2,}[a-zA-Z]+(Exception|Error)((:){0,1}[a-zA-Z \\.]*){0,1}|[ ]*(at )[a-zA-Z0-9 .():$]*|(Caused by:)|[\\. ]+[0-9 ]+(more)|(FATAL EXCEPTION:))");
		
		
		while (matcher.find()) {

			String code = matcher.group(1);
			if (code.contains(" ") || code.contains("\\.")) {
				
				// split at line breaks
				String[] lines = code.split("&#xA;");
				
				// check each line, if it is a logcat entry (they cause exception in the baker-tool
				for(String s : lines) {
					Matcher logcatMatcher = logcatRegex.matcher(s.trim());
					Matcher stackTraceMatcher = stackTraceRegex.matcher(s.trim());
					
					if(!logcatMatcher.find() && !stackTraceMatcher.find()){
						codeOfBody += s + "\n";
					}
				}
				
				codeOfBody = codeOfBody.replace("&#xA;", "\n");
				codeOfBody = codeOfBody.replace("&gt;", ">");
				codeOfBody = codeOfBody.replace("&lt;", "<");
			}

		}
		
//		System.out.println(codeOfBody);
		
		return codeOfBody;
	}
}
