package worker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

public class DBStore {
    
    private Connection conn;
	
	public DBStore(String url, String dbName, String userName, String password, String driver) {
		try {
			Class.forName(driver).newInstance();
			this.conn = DriverManager.getConnection(url + dbName, userName, password);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void storePost(StackOverflowPost p) {
		PreparedStatement insertPostStatement = null;
		// insert
		try {

			insertPostStatement = conn.prepareStatement("INSERT IGNORE INTO post2api (postid, foundMinIn, foundMaxIn, minAPI, maxAPI) VALUES (" + p.getPostID() + ", " + p.getFoundMinIn() + "," + p.getFoundMaxIn() + ", " + p.getMinVersion() + ", '" + p.getMaxVersion() + "');");
			
			insertPostStatement.executeUpdate();
			insertPostStatement.close();
			

		} catch (MySQLIntegrityConstraintViolationException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
