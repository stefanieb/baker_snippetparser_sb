package worker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.exceptions.jdbc4.MySQLNonTransientConnectionException;

import worker.StackOverflowPost;

public class PostLoader {

    private Connection conn;
    private Statement stmt;
    private ResultSet result;
    
    public PostLoader(String url, String dbName, String userName, String password, String driver) {
		try {
			Class.forName(driver).newInstance();
			this.conn = DriverManager.getConnection(url + dbName, userName, password);
			this.stmt = conn.createStatement();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
    
	public StackOverflowPost loadPost(int postId) {
		
		try {
			
//			if(!alreadyAnalyzed(postId)) {
	
				String question_query = "SELECT p1.Id, p1.Body, p1.Tags, p1.Title, p1.PostTypeId, p1.CreationDate, p1.AcceptedAnswerId, p2.Body AS AcceptedAnswerBody FROM filtered_posts p1 LEFT JOIN filtered_posts p2 ON p1.AcceptedAnswerId = p2.Id  WHERE p1.id='"+postId+"'";
				
				result = stmt.executeQuery(question_query);
				
				try {
					StackOverflowPost post = null;
					
					while (result.next())
					{
						post = new StackOverflowPost(result.getInt("Id"), result.getString("Body"),result.getString("Tags"), result.getString("Title"), result.getInt("PostTypeId"), result.getDate("CreationDate").toString(), result.getInt("AcceptedAnswerId"), result.getString("AcceptedAnswerBody"));
					}
					
					// post does not exist
					if(post == null) {
						return new StackOverflowPost(-1, null, null, null, null, null, null, null);
					}
					
					return post;
					
				} catch(NullPointerException e) {
					e.printStackTrace();
				}
			
//			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return new StackOverflowPost(-1, null, null, null, null, null, null, null);
		
	}

	private boolean alreadyAnalyzed(int postID) {
		
		try {

			String question_query = "SELECT (1) FROM post2api WHERE postID='"+postID+"'";
			
			ResultSet posts = stmt.executeQuery(question_query);
			
			if(posts.next()) {
				posts.close();
				
				return true;
			}
			
			posts.close();

		} catch(MySQLNonTransientConnectionException e) {
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
}
