package restAPIAccess;

import java.io.IOException;
import java.sql.SQLException;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.json.JSONObject;

import RestAPI.GraphServerAccess;

public class JavaBaker {
	
	public static String startBakerExternFile(String codeSnippet)
			throws IOException, NullPointerException, ClassNotFoundException {
		long start = System.currentTimeMillis();
		// Logger logger = new Logger();
		String input_oracle = "http://localhost:7474/db/data";
		String input_file = codeSnippet;
		int tolerance = 2;
		int max_cardinality = 2000;
		Parser parser = null;
		parser = new Parser(input_oracle, input_file, 0);
		CompilationUnit cu = parser.getCompilationUnitFromFile();
		int cutype = parser.getCuType();
		GraphServerAccess graphServer = parser.getGraph();
		if (graphServer == null) {
			System.out.println("db locked");
		}
//		System.out.println("\n\n-----------------------------------\n\n"+codeSnippet+"\n\n-----------------------------------\n\n");

		String json = vistAST(graphServer, cu, cutype, tolerance, max_cardinality).toString(3);
		// System.out.println(json);

		//System.out.println("JavaBaker total run: " + (System.currentTimeMillis() - start)/1000 + "s");
		// System.out.println("JavaBaker total run: "+ (end-start));
		// logger.printAccessTime("JavaBaker total run: ", " ", end, start);
		// graphServer.logger.printMap();

		return json;
	}

	public static String startBakerExternBody(String codeSnippet)
			throws IOException, NullPointerException, ClassNotFoundException {
		long start = System.currentTimeMillis();
		// Logger logger = new Logger();
		String input_oracle = "http://localhost:7474/db/data";
		String input_file = codeSnippet;
		int tolerance = 2;
		int max_cardinality = 2000;
		Parser parser = null;
		parser = new Parser(input_oracle, input_file, 1);
		CompilationUnit cu = parser.getCompilationUnitFromFile();
		int cutype = parser.getCuType();
		GraphServerAccess graphServer = parser.getGraph();
		if (graphServer == null) {
			System.out.println("db locked");
		}
		
		//System.out.println("\n\n-----------------------------------\n\n"+codeSnippet+"\n\n-----------------------------------\n\n");

		String json = vistAST(graphServer, cu, cutype, tolerance, max_cardinality).toString(3);
		// System.out.println(json);

		//System.out.println("JavaBaker total run: " + (System.currentTimeMillis() - start)/1000 + "s");
		// System.out.println("JavaBaker total run: "+ (end-start));
		// logger.printAccessTime("JavaBaker total run: ", " ", end, start);
		// graphServer.logger.printMap();

		return json;
	}

	public static void main(String[] args) throws IOException, NullPointerException, ClassNotFoundException, SQLException {

		long start = System.currentTimeMillis();
		
		// Logger logger = new Logger();
		String input_oracle = "http://localhost:7474/db/data";
		String input_file = "code_snippets/snippet_test.txt";
		int tolerance = 2; // default 2
		int max_cardinality = 2000;
		Parser parser = null;
		
		System.out.println("parsing " + input_file);
		
		if (args.length == 0) {
			parser = new Parser(input_oracle, input_file, 0);
		} else if (args.length == 1) { //Path to file passed as Argument
			input_file = args[0];
			parser = new Parser(input_oracle, input_file, 0);
		} else {
			System.out.println("Invalid arguments");
		}
		
		CompilationUnit cu = parser.getCompilationUnitFromFile();
		int cutype = parser.getCuType();
		GraphServerAccess graphServer = parser.getGraph();
		if (graphServer == null) {
			System.out.println("db locked");
		}
		
		System.out.println(vistAST(graphServer, cu, cutype, tolerance, max_cardinality).toString(3));
		//vistAST(graphServer, cu, cutype, tolerance, max_cardinality).toString(3);

		//System.out.println("JavaBaker total run: " + (System.currentTimeMillis() - start)/1000 + "s");
		// logger.printAccessTime("JavaBaker total run: ", " ", end, start);
		// graphServer.logger.printMap();
	}

	static JSONObject vistAST(GraphServerAccess db, CompilationUnit cu, int cutype, int tolerance, int max_cardinality) {
		PrefetchCandidates prefetch_visitor = new PrefetchCandidates(db, cu, cutype, tolerance, max_cardinality);
		cu.accept(prefetch_visitor);
		prefetch_visitor.classFetchExecutor.shutdown();
		prefetch_visitor.methodFetchExecutor.shutdown();
		while (prefetch_visitor.classFetchExecutor.isTerminated() == false
				|| prefetch_visitor.methodFetchExecutor.isTerminated() == false) {

		}
		
		System.out.println("FirstASTVisitor");
		FirstASTVisitor first_visitor = new FirstASTVisitor(prefetch_visitor);
		cu.accept(first_visitor);
		//first_visitor.printFields();

//		System.out.println("SubsequentASTVisitor 1");
		SubsequentASTVisitor second_visitor = new SubsequentASTVisitor(first_visitor);
		cu.accept(second_visitor);

//		System.out.println("SubsequentASTVisitor 2");
		SubsequentASTVisitor third_visitor = new SubsequentASTVisitor(second_visitor);
		cu.accept(third_visitor);
		//System.out.println(third_visitor.printJson().toString(3));
		//third_visitor.printFields();

		SubsequentASTVisitor previous_visitor = second_visitor;
		SubsequentASTVisitor current_visitor = third_visitor;

//		System.out.println("compareMaps");
		while (compareMaps(current_visitor, previous_visitor) == false) {
			SubsequentASTVisitor new_visitor = new SubsequentASTVisitor(current_visitor);
			cu.accept(new_visitor);
			previous_visitor = current_visitor;
			current_visitor = new_visitor;
		}
		
//		System.out.println("updateBasedOnImports");
		current_visitor.updateBasedOnImports();
		// current_visitor.removeClustersIfAny();
		
//		System.out.println("setJson");
		current_visitor.setJson();
		
		return current_visitor.getJson();
	}

	private static boolean compareMaps(SubsequentASTVisitor curr, SubsequentASTVisitor prev) {
		if (curr.variableTypeMap.equals(prev.variableTypeMap)
				&& curr.methodReturnTypesMap.equals(prev.methodReturnTypesMap)
				&& curr.printtypes.equals(prev.printtypes) && curr.printmethods.equals(prev.printmethods)
				&& curr.printTypesMap.equals(prev.printTypesMap) && curr.printMethodsMap.equals(prev.printMethodsMap))
			return true;
		else
			return false;
	}
}