package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import worker.DBStore;
import worker.PostLoader;
import worker.StackOverflowPost;

public class PostLoaderTest {

	private PostLoader pl;
	private StackOverflowPost post;
	
	@Before
	public void init() {
//		this.pl = new PostLoader();
	}
	
	@Test
	public void loadNonExistingPost() {
		post = pl.loadPost(123);
		
		assertEquals(post.getPostID().intValue(), -1);
	}
	
	@Test
	public void loadPostGetID() {
		post = pl.loadPost(28533557);
		
		assertEquals(post.getPostID().intValue(), 28533557);
	}
	
	@Test
	public void loadPostGetTitle() {
		post = pl.loadPost(28533557);
		
		assertEquals(post.getTitle(), "Custom adapter and click on item in ListView");
	}
	
	@Test
	public void loadPostGetTags() {
		post = pl.loadPost(28533557);
		
		assertEquals(post.getTags(), "<android>");
	}
	
	@Test
	public void loadPostAlreadyAnalyzed() {
		
		post = pl.loadPost(38768746);
		
		// if post was not analyzed, analyze it
		if(post.getPostID() > 0) {
//			DBStore db = new DBStore();
			
//			db.storePost(post);
		}
		
		post = pl.loadPost(38768746);
		
		assertEquals(post.getPostID().intValue(), -1);
	}

}
