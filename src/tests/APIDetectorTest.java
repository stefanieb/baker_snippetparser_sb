package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import worker.APIDetector;
import worker.PostLoader;
import worker.StackOverflowPost;

public class APIDetectorTest {
	private StackOverflowPost post;
	private PostLoader pl;
	
	@Before
	public void init() {
//		this.pl = new PostLoader();
	}
	
	@Test
	public void analyzePost() {
				
		post = pl.loadPost(40083673);
		
		APIDetector ad = new APIDetector(post, 0);
		
		assertEquals(post.getPostID().intValue(), -1);
	}

}
