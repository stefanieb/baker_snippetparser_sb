package compare;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONComparator {

	private final String PATH_TO_FILES = "/Users/stefanie/Documents/workspace_baker/JavaSnippetParser_sb/data/";

	private List<String> steffiFiles;
	private List<String> sidFiles;

	public static void main(String args[]) {

		new JSONComparator().compare();

	}

	public void compare() {

		//
		initFilesToCompare();
		int countSame = 0;
		if (steffiFiles.size() == sidFiles.size()) {
			System.out.println("same size (" + steffiFiles.size() + ") : continue");

			int size = steffiFiles.size();

			for (int i = 0; i < size; i++) {

				String steffiFile = steffiFiles.get(i);
				String sidFile = sidFiles.get(i);

				if (compareFiles(steffiFile, sidFile)) {
					countSame++;
				} else {
					System.out.println(sidFile.substring(sidFile.lastIndexOf("D") + 1));
				}

			}
			System.out.println(countSame + " of " + size + " results are the same");

		} else {
			System.err.println("lists do not have the same size ---> EXIT");
			System.out.println("\n" + steffiFiles.size() + " --- " + sidFiles.size());

			printDiffFiles(steffiFiles, sidFiles);
		}

	}

	private void printDiffFiles(List<String> steffiFiles, List<String> sidFiles) {
		List<String> moreFiles = steffiFiles;
		List<String> lessFiles = sidFiles;
		String name = "STEFFI";
		String name2 = "SID";
		String idx = "I";
		if (steffiFiles.size() < sidFiles.size()) {
			moreFiles = sidFiles;
			lessFiles = steffiFiles;
			name2 = "STEFFI";
			name = "SID";
			idx = "D";
		}
		int count = 0;
		for (String s1 : moreFiles) {

			if (!steffiFiles.contains(s1.replace(name, name2))) {
				System.out.println(s1.substring(s1.lastIndexOf(idx) + 1));
				count++;
			}
		}
		if (moreFiles.size() != (lessFiles.size() + count)) {
			System.out.println("TODO umgekehrt auch noch mal durchiterieren");
		}

	}

	private boolean compareFiles(String steffiFile, String sidFile) {

		// String steffiText = getTextOfFile(steffiFile);
		// String sidText = getTextOfFile(sidFile);

		// evtl hier vereinfachen
		JSONObject steffiJSON = getJSONObjectOfFile(steffiFile);
		JSONObject sidJSON = getJSONObjectOfFile(sidFile);

		return compareJSONObjects(steffiJSON, sidJSON);

	}

	private boolean compareJSONObjects(JSONObject steffiObj, JSONObject sidObj) {
		List<APIElement> steffiList = convertJSONObjectToAPIElementList(steffiObj);
		List<APIElement> sidList = convertJSONObjectToAPIElementList(sidObj);

		if (steffiList.size() == sidList.size()) {
			int size = steffiList.size();
			for (int i = 0; i < size; i++) {

				APIElement steffiElement = steffiList.get(i);
				APIElement sidElement = sidList.get(i);

				if (!steffiElement.compareTo(sidElement)) {
					return false;
				}

			}
			return true;
		}
		System.err.println("not the same size: " + steffiList.size() + " vs. " + sidList.size());

		return false;
	}

	public List<APIElement> convertJSONObjectToAPIElementList(JSONObject obj) {

		List<APIElement> returnList = new ArrayList<>();

		if (obj.get("api_elements").getClass().getSimpleName().equals("JSONArray")) {

			JSONArray arr = (JSONArray) obj.get("api_elements");
			Iterator<JSONObject> iterator = arr.iterator();
			while (iterator.hasNext()) {
				JSONObject api_element = iterator.next();
				APIElement e1 = new APIElement();
				e1.setCharacter(Integer.parseInt((String) api_element.get("character")));
				e1.setLine_number(Integer.parseInt((String) api_element.get("line_number")));
				e1.setPrecision(Integer.parseInt((String) api_element.get("precision")));
				e1.setName((String) api_element.get("name"));
				e1.setType((String) api_element.get("type"));
				e1.setElements((JSONArray) api_element.get("elements"));
				returnList.add(e1);
			}

		} else if (obj.get("api_elements").getClass().getSimpleName().equals("JSONObject")) {

			JSONObject api_element = (JSONObject) obj.get("api_elements");
			APIElement e1 = new APIElement();
			e1.setCharacter(Integer.parseInt((String) api_element.get("character")));
			e1.setLine_number(Integer.parseInt((String) api_element.get("line_number")));
			e1.setPrecision(Integer.parseInt((String) api_element.get("precision")));
			e1.setName((String) api_element.get("name"));
			e1.setType((String) api_element.get("type"));
			e1.setElements((JSONArray) api_element.get("elements"));
			returnList.add(e1);
		}

		return returnList;

	}

	private JSONObject getJSONObjectOfFile(String path) {

		JSONObject jsonObj = null;
		JSONParser parser = new JSONParser();
		// System.out.println(path);
		try {

			jsonObj = (JSONObject) parser.parse(new InputStreamReader(new FileInputStream(path)));

		} catch (ParseException e) {
			
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return jsonObj;

	}

	private void initFilesToCompare() {

		steffiFiles = new ArrayList<>();
		sidFiles = new ArrayList<>();

		File path = new File(PATH_TO_FILES);
		File[] fileList = path.listFiles();
		int i = 0;
		// boolean order = false;
		for (File file : fileList) {
			i++;
			if (i >= 0) {
				String fname = file.getAbsolutePath();

				if ((fname.contains("STEFFI")) && !fname.endsWith("code.txt")) {
					steffiFiles.add(fname);

				} else if ((fname.contains("SID")) && !fname.endsWith("code.txt")) {
					sidFiles.add(fname);
				}
			}
		}
	}

}
