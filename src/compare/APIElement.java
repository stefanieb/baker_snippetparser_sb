package compare;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;

public class APIElement {

	private int character;
	private int line_number;
	private List<String> elements;
	private int precision;
	private String name;
	private String type;
	private double version;

	public APIElement() {
		elements = new ArrayList<>();
	}

	public boolean compareTo(APIElement e1) {
		char c = 'c';

		if (this.character == e1.getCharacter()) {
			c = 'l';
			if (this.line_number == e1.getLine_number()) {
				c = 'p';
				if (this.precision == e1.getPrecision()) {
					c = 'n';
					if (this.name.equals(e1.getName())) {
						c = 't';
						if (this.type.equals(e1.getType())) {
							c = 'e';
							if (containsSameElements(e1.getElements())) {
								c = '-';
								return true;
							}

						}
					}
				}
			}
		}
		System.err.println("--- Fehler beim Vergleichen: (" + this.type + " - " + this.character + " - " + c + ") ---");

		return false;

	}

	private boolean containsSameElements(List<String> elements) {
		if (elements.size() == this.elements.size()) {

			int countTrue = 0;
			for (String e1 : elements) {
				for (String e : this.elements) {
					if (e1.equals(e)) {
						countTrue++;
						break;
					}
				}
			}
			if (countTrue == elements.size()) {
				return true;
			}
		}

		return false;

	}

	public int getCharacter() {
		return character;
	}

	public void setCharacter(int character) {
		this.character = character;
	}

	public int getLine_number() {
		return line_number;
	}

	public void setLine_number(int line_number) {
		this.line_number = line_number;
	}

	public List<String> getElements() {
		return elements;
	}

	@SuppressWarnings("unchecked")
	public void setElements(JSONArray jsonArray) {

		jsonArray.forEach((it) -> elements.add((String) it));
	}

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getVersion() {
		return version;
	}

	public void setVersion(double version) {
		this.version = version;
	}

}
