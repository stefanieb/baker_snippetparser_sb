package extractor;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import restAPIAccess.JavaBaker;

public class BakerTesterSteffi {

	private final int LIMIT = 2000;
	private final static String EMPTY_JSON = "{\"api_elements\": [{ \"precision\": \"\",\"name\": \"\",\"line_number\": \"\",\"type\": \"\",\"elements\": \"\"}]}";
	private StackToBaker sotb;
	private final String PATH_ADDON = "data/code.txt";
	int postCount;
	int skipCount;

	public static void main(String[] args) {

		BakerTesterSteffi bt = new BakerTesterSteffi();

	}

	public BakerTesterSteffi() {

		// 50 posts von DB holen
		sotb = new StackToBaker();
		List<StackOverflowPost> posts = sotb.getQuestionsFromDb(0,50);
		posts.add(sotb.getSinglePostFromDb(455830));
		posts.add(sotb.getSinglePostFromDb(1139025));
		posts.add(sotb.getSinglePostFromDb(1217162));
		posts.add(sotb.getSinglePostFromDb(1471072));

		System.out.println("POSTS: " + posts.size());

		testSteffi(posts);
		System.out.println("server ausschalten und einschalten!!");

		System.out.println("POSTCOUNT:" + postCount);
		System.out.println("SKIPCOUNT:" + skipCount);

	}

	private void testSteffi(List<StackOverflowPost> posts) {

		List<StackOverflowPost> posts2 = iterateThroughPosts(posts);
		// List<StackOverflowPost> posts3 = iterateThroughPosts(posts2);
		// List<StackOverflowPost> posts4 = iterateThroughPosts(posts3);

	}

	private List<StackOverflowPost> iterateThroughPosts(List<StackOverflowPost> posts) {
		List<StackOverflowPost> toSearchAgain = new ArrayList<>();

		for (StackOverflowPost post : posts) {
			Integer postId = post.getId();

			String body = post.getBody();
			List<String> codeSnippets = sotb.extractCode(body);
			boolean found = false;
			if (!codeSnippets.isEmpty()) {
				int count = 0;

				for (String code : codeSnippets) {

					if (!code.equals("")) {
						// System.out.println("============ " + postId + "(" +
						// count
						// + ") ==============\n" + code);
						saveIntoCodeFile(code);

						if (testJSONfromSteffiBaker(PATH_ADDON, postId, count)) {
							found = true;
						}
						count++;
						// System.out.println("==========================");
					}
				}

			}
			if (found) {
				postCount++;
			} else {
				skipCount++;
				toSearchAgain.add(post);
			}

		}
		return toSearchAgain;
	}

	private String saveIntoCodeFile(String code) {
		String filename = "/Users/stefanie/Documents/workspace_baker/JavaSnippetParser_sb/data/code.txt";
		FileWriter fw;
		try {
			fw = new FileWriter(filename, false);

			fw.write(code);
			fw.close();

		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return filename;

	}

	private void saveIntoFile(Integer postId, String json, String prefix, int count) {
		String filename = "/Users/stefanie/Documents/workspace_baker/JavaSnippetParser_sb/data/json"
				+ prefix.toUpperCase() + postId + "_" + count + ".txt";
		FileWriter fw;
		try {
			fw = new FileWriter(filename, false);

			fw.write(json);
			fw.close();

		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	private boolean testJSONfromSteffiBaker(String body, Integer postId, int count) {

		String json = "";
		try {
			json = JavaBaker.startBakerExternFile(body);
			json = json.replaceAll("<br>", "");
		} catch (IllegalArgumentException e) {
			System.out.println("catched IllegalArgumentException --> SKIP: " + postId);
			// e.printStackTrace();
			json = "";
		} catch (NullPointerException e) {
			System.out.println("catched NullPointerException --> SKIP" + postId);
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}

		// skip if emptyJSOn
		String jsonTest = json.replaceAll("\"", "-").replaceAll("\n", "n");

		if (!json.equals(EMPTY_JSON) && !json.equals("") && !jsonTest.equals("{n   -api_elements-: [n]}")) {
			saveIntoFile(postId, json, "steffi", count);
			// System.out.println(json);
			return true;
		}
		return false;

	}

}
