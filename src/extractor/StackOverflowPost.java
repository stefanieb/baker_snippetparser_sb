package extractor;

public class StackOverflowPost {

	private Integer id;
	private String body;
	private String tags;
	private String title;
	private Integer type;
	private String creationDate;
	private Integer acceptedAnswerId;
	private int foundMinIn;
	private int foundMaxIn;
	private int minAPI;
	private int maxAPI;
	
	/*
	 * Title:
	 * 1 ... question
	 * 2 ... answer
	 */

	public StackOverflowPost(Integer id, String body, String tags, String title, Integer type, String cdate, Integer acceptedAnswerId) {
		super();
		this.id = id;
		this.body = body;
		this.tags = tags;
		this.title = title;
		this.type = type;
		this.creationDate = cdate;
		this.acceptedAnswerId = acceptedAnswerId;
		this.foundMinIn = -1;
		this.foundMaxIn = -1;
		this.minAPI = 99;
		this.maxAPI = -1;
		
	}
	
	public Integer getAcceptedAnswerId() {
		return acceptedAnswerId;
	}

	public void setAcceptedAnswerId(Integer acceptedAnswerId) {
		this.acceptedAnswerId = acceptedAnswerId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getFoundMinIn() {
		return foundMinIn;
	}

	public void setFoundMinIn(int foundMinIn) {
		this.foundMinIn = foundMinIn;
	}

	public int getFoundMaxIn() {
		return foundMaxIn;
	}

	public void setFoundMaxIn(int foundMaxIn) {
		this.foundMaxIn = foundMaxIn;
	}

	public int getMinAPI() {
		return minAPI;
	}

	public void setMinAPI(int minAPI) {
		this.minAPI = minAPI;
	}

	public int getMaxAPI() {
		return maxAPI;
	}

	public void setMaxAPI(int maxAPI) {
		this.maxAPI = maxAPI;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}
