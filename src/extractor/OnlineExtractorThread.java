package extractor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class OnlineExtractorThread extends Thread {

	private final static Charset ENCODING = StandardCharsets.UTF_8;
	private String url = "jdbc:mysql://localhost:3306/";
	private String dbName = "SO_Nov2014";
	private String driver = "com.mysql.jdbc.Driver";
	private String userName = "root";
	private String password = "";
	private final static String emptyJSON = "{\"api_elements\": [{ \"precision\": \"\",\"name\": \"\",\"line_number\": \"\",\"type\": \"\",\"elements\": \"\"}]}";

	private int postCount = 0;
	private int insertCount = 0;
	private int skipCount = 0;

	private String threadName = "name not yet set";

	private String range = "0, 1";

	public void run() {

		long start = System.currentTimeMillis();

		// get 20 posts from database
		List<StackOverflowPost> postsInformationMap = getPostsFromDb();
		int count = 0;
		for (StackOverflowPost post : postsInformationMap) {

			Integer postId = post.getId();

			String title = post.getTitle();
			// System.out.println("TITLE: " + title);
			String body = post.getBody();
			getCodeFromPosts(body);

			String json = "";
			// get data from webservice in JSON format
			json = getJSONfromWebservice();

			// json =
			// moe.readDataFromFile("/Users/stefanie/Documents/Uni/json.txt");

			// skip if emptyJSOn
			if (!json.equals(emptyJSON) && !json.equals("")) {
				parseJSONandUpdateDB(postId, json);
				System.out.println("found " + count++);
			} else {
				// System.out.println("EMPTY JSON - SKIP");

				// write not used Ids to File
				saveUsedIdToFile(postId);
				skipCount++;
			}

		}

		System.out.println(threadName + " DURATION in seconds: " + ((System.currentTimeMillis() - start) / 1000));
		System.out.println(threadName + " POSTS: " + postCount);
		System.out.println(threadName + " UPDATES: " + insertCount);
		System.out.println(threadName + " SKIP: " + skipCount);

	}

	private void saveUsedIdToFile(Integer postId) {
		try {

			FileWriter writer = new FileWriter("onlineExtractor/usedIds.txt", true);

			writer.append(postId.toString());
			writer.append("\n");

			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private String getAlreadyUsedIds() {
		StringBuilder sb = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader("onlineExtractor/usedIds.txt"))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {

				sb.append(sCurrentLine);
				sb.append(",");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		String returnString = sb.toString();
		return returnString.substring(0, sb.length() - 1);

	}

	private List<StackOverflowPost> getPostsFromDb() {

		List<StackOverflowPost> postList = new ArrayList<>();

		String alreadyUsedIds = getAlreadyUsedIds();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet posts = st.executeQuery(
					"SELECT Id,Title,Body,Tags, PostTypeId FROM all_posts WHERE (Tags like '%android%' and Body like '%<code>%</code>%' and Id not in (select postid from elements2posts ) and Id not in("
							+ alreadyUsedIds + ")) order by rand() LIMIT 0, 3000;");
			// order by ViewCount desc ");

			HashMap<Integer, List<String>> returnList = new HashMap<>();

			ArrayList<String> data = null;
			while (posts.next()) {
				data = new ArrayList<>();

				// returnList.add(posts.getString("Id"));

				StackOverflowPost post = new StackOverflowPost(posts.getInt("Id"), posts.getString("Body"), 
						posts.getString("Tags"), posts.getString("Title"), posts.getInt("PostTypeId"), posts.getString("CreationDate"), posts.getInt("AcceptedAnswerId"));

				data.add(posts.getString("Title"));
				data.add(posts.getString("Body"));
				data.add(posts.getString("Tags"));

				returnList.put(posts.getInt("Id"), data);
				postList.add(post);
			}

			conn.close();

			return postList;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return null;

	}

	private void getCodeFromPosts(String body) {

		// body = readDataFromFile("/Users/stefanie/Documents/Uni/json.txt");
		// System.out.println("BODY: " + body);
		String codeOfBody = "";

		// parse body and extract code snippets or

		Pattern pattern = Pattern.compile("<code>(.+?)</code>");
		Matcher matcher = pattern.matcher(body);
		while (matcher.find())
			codeOfBody += matcher.group(1) + "\n";

		codeOfBody = codeOfBody.replace("&#xA;", "\n");
		codeOfBody = codeOfBody.replace("&gt;", ">");
		codeOfBody = codeOfBody.replace("&lt;", "<");
		// System.out.println(codeOfBody);

		// SAVE IN FILE

		String filename = "/Users/stefanie/Documents/Uni/code.txt";
		FileWriter fw;
		try {
			fw = new FileWriter(filename, false);

			fw.write(codeOfBody);
			fw.close();

		} catch (IOException e) {
			
			e.printStackTrace();
		}
		postCount++;

	}

	private String getJSONfromWebservice() {
		Process ls = null;
		BufferedReader input = null;
		String line = null;

		long now = System.currentTimeMillis();
		// System.out.println("TIME " + now);

		try {

			ls = Runtime.getRuntime().exec(new String[] { "curl", "--data-urlencode", "pastedcode@/Users/stefanie/Documents/Uni/code.txt",
					"http://awenda.cs.uwaterloo.ca/snippet/getapijsonfromcode.php" });
			input = new BufferedReader(new InputStreamReader(ls.getInputStream()));

		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		}

		try {
			String json = "";
			while ((line = input.readLine()) != null) {
				line = line.replace("<br>", "");
				json += line;

				// System.out.println(line);
			}

			// System.out.println("TIME: " + ((System.currentTimeMillis() - now) / 1000));
			return json;

		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(0);
		}
		return null;

	}

	private String readDataFromFile(String pathToFile) {
		Path path = Paths.get(pathToFile);
		try (BufferedReader reader = Files.newBufferedReader(path, ENCODING); LineNumberReader lineReader = new LineNumberReader(reader);) {
			String line = null;
			String fileToString = "";

			while ((line = lineReader.readLine()) != null) {

				fileToString += line;

			}
			return fileToString;

		} catch (IOException ex) {
			ex.printStackTrace();

		}

		return null;
	}

	private void parseJSONandUpdateDB(int id_post, String json) {

		Object obj = JSONValue.parse(json);

		JSONObject jsonObject = (JSONObject) obj;

		// if (jsonObject.getClass().equals(JSONArray.class) &&
		// jsonObject.entrySet().size() == 1) {
		// return;
		// }

		Object o = jsonObject.get("api_elements");
		if (o instanceof JSONObject) {
			JSONObject oo = (JSONObject) jsonObject.get("api_elements");
			handleJSON(id_post, oo);
		} else if (o instanceof JSONArray) {
			JSONArray array = (JSONArray) jsonObject.get("api_elements");

			for (Iterator<?> iter = array.iterator(); iter.hasNext();) {
				JSONObject oo = (JSONObject) iter.next();
				handleJSON(id_post, oo);

			}
		}

	}

	private void handleJSON(int id_post, JSONObject oo) {
		JSONArray elements = (JSONArray) oo.get("elements");

		Integer precision = new Integer(oo.get("precision").toString());
		String line_number = oo.get("line_number").toString();

		for (Object element : elements) {

			String fqn = element.toString();

			// exists element in db?
			int element_id = getIdOfElementByFQN(fqn);

			// 0 = no, x>0 = yes, -1 = error
			if (element_id == 0) {

				// wenn nein: anlegen + id holen
				element_id = storeElement(oo.get("type").toString(), fqn);

			} else if (element_id == -1) {
				System.err.println("ERROR IN SQL-Abfrage");
			}

			// if (oo.get("type").toString().equals("api_method") && fqn.startsWith("android")) {

			storeElement2Posts(id_post, precision, oo.get("name").toString(), line_number, element_id, oo.get("type").toString(), fqn);
			// }

			// save
			// storeElement2Posts(id_post, precision, oo.get("name").toString(),
			// line_number, element_id, oo.get("type").toString(), fqn);

		}

		// JSONArray ar = (JSONArray) oo.get("elements");
	}

	private void storeElement2Posts(int id_post, int precision, String name, String line, int element_id, String type, String elementFQN) {
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			Integer id_class = null;

			String values = id_post + ", " + precision + ", '" + name + "', '" + line + "', '" + element_id;

			// update classes2posts set post_count_
			PreparedStatement updateDataStatement = conn
					.prepareStatement("insert into elements2posts (postid, _precision, name, line, element_id) values (" + values + "');");
			int res = updateDataStatement.executeUpdate();
			insertCount++;
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

	}

	private int storeElement(String type, String fqn) {
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			// insert new element
			PreparedStatement updateDataStatement = conn.prepareStatement("insert into elements (type, fqn) values ('" + type + "', '" + fqn + "');");
			int res = updateDataStatement.executeUpdate();
			insertCount++;
			conn.close();

			return getIdOfElementByFQN(fqn);

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return -1;

	}

	private int getIdOfElementByFQN(String fqn) {
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			// if element exists, id is returned, otherwise 0
			int elementId = 0;
			ResultSet result = st.executeQuery("select id from elements where elements.fqn='" + fqn + "';");
			if (result.next()) {
				elementId = result.getInt("id");
			}
			conn.close();
			return elementId;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return -1;

	}

	public String getThreadName() {
		return threadName;
	}

	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

}
