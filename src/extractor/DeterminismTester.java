package extractor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import compare.APIElement;
import restAPIAccess.JavaBaker;

public class DeterminismTester {

	private int LIMIT = 50;
	private int postCount;
	private int skipCount;
	private final static String EMPTY_JSON = "{\"api_elements\": [{ \"precision\": \"\",\"name\": \"\",\"line_number\": \"\",\"type\": \"\",\"elements\": \"\"}]}";
	private final String PATH_ADDON = "data/code.txt";

	private List<String> r1Files;
	private List<String> r2Files;

	public static void main(String args[]) {
		for (int i = 0; i < 20; i++) {
			new DeterminismTester();
		}
		System.out.println("=== FINISHED ===");
	}

	public DeterminismTester() {

		saveFiles();
		if (compareFiles()) {
			System.out.println("OK");
		} else {
			System.out.println("NOK");
		}

		System.out.println("=======================================");
	}

	private boolean compareFiles() {
		initFilesToCompare();
		int countSame = 0;
		if (r1Files.size() == r2Files.size()) {
			System.out.println("same size: continue");

			int size = r1Files.size();

			for (int i = 0; i < size; i++) {

				String r1F = r1Files.get(i);
				String r2F = r2Files.get(i);

				if (compareFiles(r1F, r2F)) {
					countSame++;
				} else {
					System.out.println(r2F);
				}

			}
			System.out.println(countSame + " of " + size + " results are the same");
			return true;
		} else {
			System.err.println("lists do not have the same size ---> EXIT");
			System.out.println("\n" + r1Files.size() + " --- " + r2Files.size());

			printDiffFiles(r1Files, r2Files);
		}

		return false;
	}

	private boolean compareFiles(String steffiFile, String sidFile) {

		// String steffiText = getTextOfFile(steffiFile);
		// String sidText = getTextOfFile(sidFile);

		// evtl hier vereinfachen
		JSONObject steffiJSON = getJSONObjectOfFile(steffiFile);
		JSONObject sidJSON = getJSONObjectOfFile(sidFile);

		return compareJSONObjects(steffiJSON, sidJSON);

	}

	private JSONObject getJSONObjectOfFile(String path) {

		JSONObject jsonObj = null;
		JSONParser parser = new JSONParser();
		// System.out.println(path);
		try {

			jsonObj = (JSONObject) parser.parse(new InputStreamReader(new FileInputStream(path)));

		} catch (ParseException e) {
			
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return jsonObj;

	}

	private boolean compareJSONObjects(JSONObject steffiObj, JSONObject sidObj) {
		List<APIElement> steffiList = convertJSONObjectToAPIElementList(steffiObj);
		List<APIElement> sidList = convertJSONObjectToAPIElementList(sidObj);

		if (steffiList.size() == sidList.size()) {
			int size = steffiList.size();
			for (int i = 0; i < size; i++) {

				APIElement steffiElement = steffiList.get(i);
				APIElement sidElement = sidList.get(i);

				if (!steffiElement.compareTo(sidElement)) {
					return false;
				}

			}
			return true;
		}
		System.err.println("not the same size: " + steffiList.size() + " vs. " + sidList.size());

		return false;
	}

	public List<APIElement> convertJSONObjectToAPIElementList(JSONObject obj) {

		List<APIElement> returnList = new ArrayList<>();

		if (obj.get("api_elements").getClass().getSimpleName().equals("JSONArray")) {

			JSONArray arr = (JSONArray) obj.get("api_elements");
			Iterator<JSONObject> iterator = arr.iterator();
			while (iterator.hasNext()) {
				JSONObject api_element = iterator.next();
				APIElement e1 = new APIElement();
				e1.setCharacter(Integer.parseInt((String) api_element.get("character")));
				e1.setLine_number(Integer.parseInt((String) api_element.get("line_number")));
				e1.setPrecision(Integer.parseInt((String) api_element.get("precision")));
				e1.setName((String) api_element.get("name"));
				e1.setType((String) api_element.get("type"));
				e1.setElements((JSONArray) api_element.get("elements"));
				returnList.add(e1);
			}

		} else if (obj.get("api_elements").getClass().getSimpleName().equals("JSONObject")) {

			JSONObject api_element = (JSONObject) obj.get("api_elements");
			APIElement e1 = new APIElement();
			e1.setCharacter(Integer.parseInt((String) api_element.get("character")));
			e1.setLine_number(Integer.parseInt((String) api_element.get("line_number")));
			e1.setPrecision(Integer.parseInt((String) api_element.get("precision")));
			e1.setName((String) api_element.get("name"));
			e1.setType((String) api_element.get("type"));
			e1.setElements((JSONArray) api_element.get("elements"));
			returnList.add(e1);
		}

		return returnList;

	}

	private String saveIntoCodeFile(String code) {
		String filename = "/Users/stefanie/Documents/workspace_baker/JavaSnippetParser_sb/data/code.txt";
		FileWriter fw;
		try {
			fw = new FileWriter(filename, false);

			fw.write(code);
			fw.close();

		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return filename;

	}

	private boolean testJSONfromBaker(String body, Integer postId, int count, int round) {

		String json = "";
		try {
			json = JavaBaker.startBakerExternFile(body);
			json = json.replaceAll("<br>", "");
		} catch (IllegalArgumentException e) {
			System.out.println("catched IllegalArgumentException --> SKIP " + postId);
			// e.printStackTrace();
			json = "";
		} catch (NullPointerException e) {
			System.out.println("catched NullPointerException --> SKIP " + postId);
			// e.printStackTrace();
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}

		// skip if emptyJSOn
		String jsonTest = json.replaceAll("\"", "-").replaceAll("\n", "n");

		if (!json.equals(EMPTY_JSON) && !json.equals("") && !jsonTest.equals("{n   -api_elements-: [n]}")) {
			saveIntoFile(postId, json, "steffi", count, round);
			return true;
		}
		return false;

	}

	private void saveIntoFile(Integer postId, String json, String prefix, int count, int round) {
		String filename = "/Users/stefanie/Desktop/testDT/json" + prefix.toUpperCase() + postId + "_" + "r" + round
				+ "_" + count + ".txt";
		FileWriter fw;
		try {
			fw = new FileWriter(filename, false);

			fw.write(json);
			fw.close();

		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	private void initFilesToCompare() {

		r1Files = new ArrayList<>();
		r2Files = new ArrayList<>();

		File path = new File("/Users/stefanie/Desktop/testDT/");
		File[] fileList = path.listFiles();
		int i = 0;
		// boolean order = false;
		for (File file : fileList) {
			i++;
			if (i >= 0) {
				String fname = file.getAbsolutePath();

				if ((fname.contains("r1")) && !fname.endsWith("code.txt")) {
					r1Files.add(fname);

				} else if ((fname.contains("r2")) && !fname.endsWith("code.txt")) {
					r2Files.add(fname);
				}
			}
		}
	}

	private void printDiffFiles(List<String> steffiFiles, List<String> sidFiles) {
		List<String> moreFiles = steffiFiles;
		List<String> lessFiles = sidFiles;
		String name = "STEFFI";
		String name2 = "SID";
		String idx = "I";
		if (steffiFiles.size() < sidFiles.size()) {
			moreFiles = sidFiles;
			lessFiles = steffiFiles;
			name2 = "STEFFI";
			name = "SID";
			idx = "D";
		}
		int count = 0;
		for (String s1 : moreFiles) {

			if (!steffiFiles.contains(s1.replace(name, name2))) {
				System.out.println(s1.substring(s1.lastIndexOf(idx) + 1));
				count++;
			}
		}
		if (moreFiles.size() != (lessFiles.size() + count)) {
			System.out.println("TODO umgekehrt auch noch mal durchiterieren");
		}

	}

	private void saveFiles() {
		StackToBaker sotb = new StackToBaker();
		List<StackOverflowPost> posts = sotb.getQuestionsFromDb(0,LIMIT);
		posts.add(sotb.getSinglePostFromDb(455830));
		posts.add(sotb.getSinglePostFromDb(1139025));
		posts.add(sotb.getSinglePostFromDb(1217162));
		posts.add(sotb.getSinglePostFromDb(1471072));

		for (int round = 1; round < 3; round++) {

			for (StackOverflowPost post : posts) {
				Integer postId = post.getId();

				String body = post.getBody();
				List<String> codeSnippets = sotb.extractCode(body);
				boolean found = false;
				if (!codeSnippets.isEmpty()) {
					int count = 0;

					for (String code : codeSnippets) {

						if (!code.equals("")) {
							// System.out.println("============ " + postId + "("
							// +
							// count
							// + ") ==============\n" + code);
							saveIntoCodeFile(code);

							if (testJSONfromBaker(PATH_ADDON, postId, count, round)) {
								found = true;
							}
							count++;
							// System.out.println("==========================");
						}
					}

				}
				if (found)
					postCount++;
				else
					skipCount++;

			}
		}
	}

}
