package extractor;

public class PostResult {

	private int postID;
	private int foundMinIn;
	private int foundMaxIn;
	private int minAPI;
	private int maxAPI;
	
	public PostResult(int pid, int foundminin, int foundmaxin, int miapi, int maapi) {
		this.postID = pid;
		this.foundMinIn = foundminin;
		this.foundMaxIn = foundmaxin;
		this.minAPI = miapi;
		this.maxAPI = maapi;
	}
	
	public int getPostID() {
		return postID;
	}
	public void setPostID(int postID) {
		this.postID = postID;
	}
	public int getFoundMinIn() {
		return foundMinIn;
	}

	public void setFoundMinIn(int foundMinIn) {
		this.foundMinIn = foundMinIn;
	}

	public int getFoundMaxIn() {
		return foundMaxIn;
	}

	public void setFoundMaxIn(int foundMaxIn) {
		this.foundMaxIn = foundMaxIn;
	}

	public int getMinAPI() {
		return minAPI;
	}
	public void setMinAPI(int minAPI) {
		this.minAPI = minAPI;
	}
	public int getMaxAPI() {
		return maxAPI;
	}
	public void setMaxAPI(int maxAPI) {
		this.maxAPI = maxAPI;
	}
	
	
	
}
