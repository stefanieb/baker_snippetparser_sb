package extractor;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.MediaType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import restAPIAccess.JavaBaker;

public class StackToBaker {
	
	// --- StackToBaker Tools -------------------------------
	//
	//	Usage:
	//  java -jar jarname mode params
	//
	//  single post mode:
	//  java -jar jarname single postID
	//
	//  post-range mode:
	//  java -jar jarname range startIndex numRowsToSelect
	//
	//  all posts mode:
	//  java -jar jarname all
	//
	// ------------------------------------------------------
	
	// enumeration for the found-status
	// found api-level in:
	//	1...post code (baker)
	//	2...post title
	//	3...post body
	//	4...post tags
	//	5...accepted answer code (baker)
	//	6...accepted answer body
	//	7...datum
	
	private enum FoundIn {
		POST_CODE(1),
		POST_TITLE(2),
		POST_BODY(3),
		POST_TAGS(4),
		ANSWER_CODE(5),
		ANSWER_BODY(6),
		POST_DATE(7);
		
		private int value;
		
		FoundIn(int val) {
			this.value = val;
		}
	}
	
	// TODO: Look at post
	// 15379443
	// 15380972
	// 1360352
	
	// MySQL Configuration:
//	private String url = "jdbc:mysql://baker1.isys.uni-klu.ac.at:3306/";
//	private String dbName = "SO2016_12";
//	private String userName = "markus";
//	private String password = "";
	
	// Local:
    private String url = "jdbc:mysql://localhost:3306/";
    private String dbName = "so2016_12";
    private String userName = "root";
    private String password = "";
	
	// JDBC-Driver and JSON-Format
	private String driver = "com.mysql.jdbc.Driver";
	private final static String emptyJSON = "{\"min_version\":\"\",\"max_version\":\"\",\"api_elements\": [{\"min_version\":\"\",\"max_version\":\"\", \"precision\": \"\",\"name\": \"\",\"line_number\": \"\",\"type\": \"\",\"elements\": \"\"}]}";
	
	// Tool Parameters
	private static int numRowsToSelect; // the number of rows to be processed in range-mode
	private static int startIndex;	    // the startindex for range-mode
	private static int passedPostID;    // the passed postId for single-post-mode
	private static int parsingMode;     // 0 ... single post | 1 ... range of posts
	
	private static int totalMinVersion = 1;	  // hard-coded total minimum API-Version  TODO: make generic / autodetect
	private static int totalMaxVersion = 25;  // hard-coded total maximumg API-Version TODO: make generic / autodetect
	
	private static boolean showOutput = true; // toggle to display/hide the console-output (no output -> faster)
	private static List<String> fqn = new ArrayList<String>();
	
	// An ArrayList with the unix timestamps of all android release-dates 
	// TODO: make generic
	private static ArrayList<Integer> androidVersionDates = new ArrayList<Integer>() {
		{
			add(1222128000);//API 1
			add(1234137600); //API 2
			add(1240790400); //API 3
			add(1252972800); //API 4
			add(1256515200); //API 5
			add(1259798400); //API 6
			add(1263254400); //API 7
			add(1274313600); //API 8
			add(1291593600); //API 9
			add(1297209600); //API 10
			add(1298332800); //API 11
			add(1304985600); //API 12
			add(1308096000); //API 13
			add(1318896000); //API 14
			add(1324857600); //API 15
			add(1341792000); //API 16
			add(1352764800); //API 17
			add(1374624000); //API 18
			add(1383177600); //API 19
			add(1403654400); //API 20
			add(1415750400); //API 21
			add(1425859200); //API 22
			add(1444003200); //API 23
			add(1471824000); //API 24
			add(1475539200); //API 25
		}
	};

	// A list with keywords and their corresponding API-levels
	// TODO: make generic
	private static HashMap<String, ArrayList<Integer>> keywordsToVersionMap = new HashMap<String, ArrayList<Integer>>(){ 
	    { 
	    	put("android-1.0",new ArrayList<Integer>(Arrays.asList(1)));
	    	put("android-1.1",new ArrayList<Integer>(Arrays.asList(2)));
	    	put("android-1.5-cupcake",new ArrayList<Integer>(Arrays.asList(3)));
	    	put("android-cupcake",new ArrayList<Integer>(Arrays.asList(3)));
	    	put("android-1.5",new ArrayList<Integer>(Arrays.asList(3)));
	    	put("android 1.0",new ArrayList<Integer>(Arrays.asList(1)));
	    	put("android 1.1",new ArrayList<Integer>(Arrays.asList(2)));
	    	put("android 1.5-cupcake",new ArrayList<Integer>(Arrays.asList(3)));
	    	put("android cupcake",new ArrayList<Integer>(Arrays.asList(3)));
	    	put("android 1.5",new ArrayList<Integer>(Arrays.asList(3)));
	    	put("cupcake",new ArrayList<Integer>(Arrays.asList(3)));
	    	put("android-1.6-donut",new ArrayList<Integer>(Arrays.asList(4)));
	    	put("android-donut",new ArrayList<Integer>(Arrays.asList(4)));
	    	put("android 1.6 donut",new ArrayList<Integer>(Arrays.asList(4)));
	    	put("android donut",new ArrayList<Integer>(Arrays.asList(4)));
	    	put("donut",new ArrayList<Integer>(Arrays.asList(4)));
	    	put("android-1.6",new ArrayList<Integer>(Arrays.asList(4)));
	    	put("android-2.0-eclair",new ArrayList<Integer>(Arrays.asList(5,6)));
	    	put("android-2.0",new ArrayList<Integer>(Arrays.asList(5,6)));
	    	put("android-2.0.1",new ArrayList<Integer>(Arrays.asList(6)));
	    	put("android-2.1-eclair",new ArrayList<Integer>(Arrays.asList(7)));
	    	put("android-2.1",new ArrayList<Integer>(Arrays.asList(7)));
	    	put("android-eclair",new ArrayList<Integer>(Arrays.asList(7)));
	    	put("android 1.6",new ArrayList<Integer>(Arrays.asList(4)));
	    	put("android 2.0 eclair",new ArrayList<Integer>(Arrays.asList(5,6)));
	    	put("android 2.0",new ArrayList<Integer>(Arrays.asList(5,6)));
	    	put("android 2.0.1",new ArrayList<Integer>(Arrays.asList(6)));
	    	put("android 2.1 eclair",new ArrayList<Integer>(Arrays.asList(7)));
	    	put("android 2.1",new ArrayList<Integer>(Arrays.asList(7)));
	    	put("android eclair",new ArrayList<Integer>(Arrays.asList(7)));
			put("eclair",new ArrayList<Integer>(Arrays.asList(5,6,7)));
	    	put("android-2.2-froyo",new ArrayList<Integer>(Arrays.asList(8)));
	    	put("android-froyo",new ArrayList<Integer>(Arrays.asList(8)));
	    	put("android-2.2",new ArrayList<Integer>(Arrays.asList(8)));
	    	put("android-2.2.1", new ArrayList<Integer>(Arrays.asList(8)));
	    	put("android-2.2.2", new ArrayList<Integer>(Arrays.asList(8)));
	    	put("android-2.2.3", new ArrayList<Integer>(Arrays.asList(8)));
	    	put("android 2.2 froyo",new ArrayList<Integer>(Arrays.asList(8)));
	    	put("android froyo",new ArrayList<Integer>(Arrays.asList(8)));
	    	put("android 2.2",new ArrayList<Integer>(Arrays.asList(8)));
	    	put("android 2.2.1", new ArrayList<Integer>(Arrays.asList(8)));
	    	put("android 2.2.2", new ArrayList<Integer>(Arrays.asList(8)));
	    	put("android 2.2.3", new ArrayList<Integer>(Arrays.asList(8)));
	    	put("froyo",new ArrayList<Integer>(Arrays.asList(8)));
	    	put("android-2.3-gingerbread",new ArrayList<Integer>(Arrays.asList(9,10)));
	    	put("android-gingerbread",new ArrayList<Integer>(Arrays.asList(9,10)));
	    	put("android-2.3",new ArrayList<Integer>(Arrays.asList(9,10)));
	    	put("android 2.3 gingerbread",new ArrayList<Integer>(Arrays.asList(9,10)));
	    	put("android gingerbread",new ArrayList<Integer>(Arrays.asList(9,10)));
	    	put("android 2.3",new ArrayList<Integer>(Arrays.asList(9,10)));
	    	put("gingerbread",new ArrayList<Integer>(Arrays.asList(9,10)));
	    	put("android-2.3.1", new ArrayList<Integer>(Arrays.asList(9)));
	    	put("android-2.3.2", new ArrayList<Integer>(Arrays.asList(9)));
	    	put("android-2.3.3", new ArrayList<Integer>(Arrays.asList(10)));
	    	put("android-2.3.4", new ArrayList<Integer>(Arrays.asList(10)));
	    	put("android-2.3.5", new ArrayList<Integer>(Arrays.asList(10)));
	    	put("android-2.3.6", new ArrayList<Integer>(Arrays.asList(10)));
	    	put("android-2.3.7", new ArrayList<Integer>(Arrays.asList(10)));
	    	put("android 2.3.1", new ArrayList<Integer>(Arrays.asList(9)));
	    	put("android 2.3.2", new ArrayList<Integer>(Arrays.asList(9)));
	    	put("android 2.3.3", new ArrayList<Integer>(Arrays.asList(10)));
	    	put("android 2.3.4", new ArrayList<Integer>(Arrays.asList(10)));
	    	put("android 2.3.5", new ArrayList<Integer>(Arrays.asList(10)));
	    	put("android 2.3.6", new ArrayList<Integer>(Arrays.asList(10)));
	    	put("android 2.3.7", new ArrayList<Integer>(Arrays.asList(10)));
	    	put("android 3.0 honeycomb",new ArrayList<Integer>(Arrays.asList(11)));
	    	put("android-honeycomb",new ArrayList<Integer>(Arrays.asList(11,12,13)));
	    	put("android-3.0",new ArrayList<Integer>(Arrays.asList(11)));
	    	put("android honeycomb",new ArrayList<Integer>(Arrays.asList(11,12,13)));
	    	put("android 3.0",new ArrayList<Integer>(Arrays.asList(11)));
	    	put("honeycomb",new ArrayList<Integer>(Arrays.asList(11,12,13)));
	    	put("android-3.0-honeycomb",new ArrayList<Integer>(Arrays.asList(11)));
	    	put("android-3.1",new ArrayList<Integer>(Arrays.asList(12)));
	    	put("android-3.2",new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android-3.2.1", new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android-3.2.2", new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android-3.2.3", new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android-3.2.4", new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android-3.2.5", new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android-3.2.6", new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android-4.0",new ArrayList<Integer>(Arrays.asList(14)));
	    	put("android-ics",new ArrayList<Integer>(Arrays.asList(14)));
	    	put("ice-cream-sandwich",new ArrayList<Integer>(Arrays.asList(14)));
	    	put("android 3.1",new ArrayList<Integer>(Arrays.asList(12)));
	    	put("android 3.2",new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android 3.2.1", new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android 3.2.2", new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android 3.2.3", new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android 3.2.4", new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android 3.2.5", new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android 3.2.6", new ArrayList<Integer>(Arrays.asList(13)));
	    	put("android 4.0",new ArrayList<Integer>(Arrays.asList(14)));
	    	put("android ics",new ArrayList<Integer>(Arrays.asList(14)));
	    	put("ice cream sandwich",new ArrayList<Integer>(Arrays.asList(14)));
	    	put("ics",new ArrayList<Integer>(Arrays.asList(14)));
	    	put("android4.0.3",new ArrayList<Integer>(Arrays.asList(15)));
	    	put("android-4.0.3",new ArrayList<Integer>(Arrays.asList(15)));
	    	put("android-4.1-jelly-bean",new ArrayList<Integer>(Arrays.asList(16)));
	    	put("android-4.2-jelly-bean",new ArrayList<Integer>(Arrays.asList(17)));
	    	put("android-4.3-jelly-bean",new ArrayList<Integer>(Arrays.asList(18)));
	    	put("jelly-bean",new ArrayList<Integer>(Arrays.asList(16,17,18)));
	    	put("android-jelly-bean",new ArrayList<Integer>(Arrays.asList(16,17,18)));
	    	put("android-4.2",new ArrayList<Integer>(Arrays.asList(18)));
	    	put("android-4.3",new ArrayList<Integer>(Arrays.asList(18)));
	    	put("android-4.4",new ArrayList<Integer>(Arrays.asList(19,20)));
	    	put("android-4.4-kitkat",new ArrayList<Integer>(Arrays.asList(19,20)));
	    	put("android 4.0.3",new ArrayList<Integer>(Arrays.asList(15)));
	    	put("android 4.1 jelly bean",new ArrayList<Integer>(Arrays.asList(16)));
	    	put("android 4.2 jelly bean",new ArrayList<Integer>(Arrays.asList(17)));
	    	put("android 4.3 jelly bean",new ArrayList<Integer>(Arrays.asList(18)));
	    	put("jelly bean",new ArrayList<Integer>(Arrays.asList(16,17,18)));
	    	put("android jelly bean",new ArrayList<Integer>(Arrays.asList(16,17,18)));
	    	put("android 4.2",new ArrayList<Integer>(Arrays.asList(18)));
	    	put("android 4.3",new ArrayList<Integer>(Arrays.asList(18)));
	    	put("android 4.4",new ArrayList<Integer>(Arrays.asList(19,20)));
	    	put("android 4.4 kitkat",new ArrayList<Integer>(Arrays.asList(19,20)));
	    	put("kitkat",new ArrayList<Integer>(Arrays.asList(19,20)));
	    	put("android-4.0.1", new ArrayList<Integer>(Arrays.asList(14)));
	    	put("android-4.0.2", new ArrayList<Integer>(Arrays.asList(14)));
	    	put("android-4.0.4", new ArrayList<Integer>(Arrays.asList(15)));
	    	put("android-4.1", new ArrayList<Integer>(Arrays.asList(16)));
	    	put("android-4.1.1", new ArrayList<Integer>(Arrays.asList(16)));
	    	put("android-4.1.2", new ArrayList<Integer>(Arrays.asList(16)));
	    	put("android-4.2.1", new ArrayList<Integer>(Arrays.asList(17)));
	    	put("android-4.2.2", new ArrayList<Integer>(Arrays.asList(17)));
	    	put("android-4.3.1", new ArrayList<Integer>(Arrays.asList(18)));
	    	put("android-4.4.1", new ArrayList<Integer>(Arrays.asList(19)));
	    	put("android-4.4.2", new ArrayList<Integer>(Arrays.asList(19)));
	    	put("android-4.4.3", new ArrayList<Integer>(Arrays.asList(19)));
	    	put("android-4.4.4", new ArrayList<Integer>(Arrays.asList(19)));
	    	put("android-4.4W", new ArrayList<Integer>(Arrays.asList(20)));
	    	put("android-4.4W.1", new ArrayList<Integer>(Arrays.asList(20)));
	    	put("android-4.4W.2", new ArrayList<Integer>(Arrays.asList(20)));
	    	put("android-5.0-lollipop",new ArrayList<Integer>(Arrays.asList(21)));
	    	put("android-5.1.1-lollipop",new ArrayList<Integer>(Arrays.asList(22)));
	    	put("android-5.0", new ArrayList<Integer>(Arrays.asList(21)));
	    	put("android-5.0.1", new ArrayList<Integer>(Arrays.asList(21)));
	    	put("android-5.0.2", new ArrayList<Integer>(Arrays.asList(21)));
	    	put("android-5.1", new ArrayList<Integer>(Arrays.asList(22)));
	    	put("android-5.1.1", new ArrayList<Integer>(Arrays.asList(22)));
	    	put("android-l",new ArrayList<Integer>(Arrays.asList(21,22)));
	    	put("android-lollipop",new ArrayList<Integer>(Arrays.asList(21,22)));
	    	put("android 4.0.1", new ArrayList<Integer>(Arrays.asList(14)));
	    	put("android 4.0.2", new ArrayList<Integer>(Arrays.asList(14)));
	    	put("android 4.0.4", new ArrayList<Integer>(Arrays.asList(15)));
	    	put("android 4.1", new ArrayList<Integer>(Arrays.asList(16)));
	    	put("android 4.1.1", new ArrayList<Integer>(Arrays.asList(16)));
	    	put("android 4.1.2", new ArrayList<Integer>(Arrays.asList(16)));
	    	put("android 4.2.1", new ArrayList<Integer>(Arrays.asList(17)));
	    	put("android 4.2.2", new ArrayList<Integer>(Arrays.asList(17)));
	    	put("android 4.3.1", new ArrayList<Integer>(Arrays.asList(18)));
	    	put("android 4.4.1", new ArrayList<Integer>(Arrays.asList(19)));
	    	put("android 4.4.2", new ArrayList<Integer>(Arrays.asList(19)));
	    	put("android 4.4.3", new ArrayList<Integer>(Arrays.asList(19)));
	    	put("android 4.4.4", new ArrayList<Integer>(Arrays.asList(19)));
	    	put("android 4.4W", new ArrayList<Integer>(Arrays.asList(20)));
	    	put("android 4.4W.1", new ArrayList<Integer>(Arrays.asList(20)));
	    	put("android 4.4W.2", new ArrayList<Integer>(Arrays.asList(20)));
	    	put("android 5.0 lollipop",new ArrayList<Integer>(Arrays.asList(21)));
	    	put("android 5.1.1 lollipop",new ArrayList<Integer>(Arrays.asList(22)));
	    	put("android 5.0", new ArrayList<Integer>(Arrays.asList(21)));
	    	put("android 5.0.1", new ArrayList<Integer>(Arrays.asList(21)));
	    	put("android 5.0.2", new ArrayList<Integer>(Arrays.asList(21)));
	    	put("android 5.1", new ArrayList<Integer>(Arrays.asList(22)));
	    	put("android 5.1.1", new ArrayList<Integer>(Arrays.asList(22)));
	    	put("android-5.1.1-lollipop", new ArrayList<Integer>(Arrays.asList(22)));
	    	put("android l",new ArrayList<Integer>(Arrays.asList(21,22)));
	    	put("android lollipop",new ArrayList<Integer>(Arrays.asList(21,22)));
			put("lollipop",new ArrayList<Integer>(Arrays.asList(21,22)));
	    	put("android-6.0-marshmallow",new ArrayList<Integer>(Arrays.asList(23)));
	    	put("android-6.0.1",new ArrayList<Integer>(Arrays.asList(23)));
	    	put("android-m",new ArrayList<Integer>(Arrays.asList(23)));
	    	put("android-marshmallow",new ArrayList<Integer>(Arrays.asList(23)));
	    	put("android 6.0 marshmallow",new ArrayList<Integer>(Arrays.asList(23)));
	    	put("android 6.0.1",new ArrayList<Integer>(Arrays.asList(23)));
	    	put("android m",new ArrayList<Integer>(Arrays.asList(23)));
	    	put("android marshmallow",new ArrayList<Integer>(Arrays.asList(23)));
	    	put("android m",new ArrayList<Integer>(Arrays.asList(23)));
			put("marshmallow",new ArrayList<Integer>(Arrays.asList(23)));
	    	put("android-6.0", new ArrayList<Integer>(Arrays.asList(23)));
	    	put("android-7.0", new ArrayList<Integer>(Arrays.asList(24)));
	    	put("android-7.1", new ArrayList<Integer>(Arrays.asList(25)));
	    	put("android-7.0-nougat",new ArrayList<Integer>(Arrays.asList(24)));
	    	put("android-7.1-nougat",new ArrayList<Integer>(Arrays.asList(25)));
	    	put("android 6.0", new ArrayList<Integer>(Arrays.asList(23)));
	    	put("android 7.0", new ArrayList<Integer>(Arrays.asList(24)));
	    	put("android 7.1", new ArrayList<Integer>(Arrays.asList(256)));
	    	put("android 7.0 nougat",new ArrayList<Integer>(Arrays.asList(24)));
	    	put("android 7.1 nougat",new ArrayList<Integer>(Arrays.asList(25)));
			put("nougat",new ArrayList<Integer>(Arrays.asList(24,25)));
			put("android n",new ArrayList<Integer>(Arrays.asList(24,25)));
	    }
	};
	
	public static void main(String args[]) {
		
		// check the passed arguments
		if(args.length < 1){
			
	    	System.err.println(" - Wrong arguments - ");
	    	System.err.println("   start with params:");
	    	System.err.println("   java -jar jarname single postID");
	    	System.err.println("   OR");
	    	System.err.println("   java -jar jarname range startIndex numRowsToSelect");
	        System.exit(1);
	        
		} else {
			
			// parse the arguments
			// analyze a single post
			if(args[0].equals("single")) {
				
				try{
					parsingMode = 0;
					passedPostID = Integer.parseInt(args[1]);
				}catch(NumberFormatException e){
			    	System.err.println("Wrong Arguments - Exiting now");
			        System.exit(1);
			    }
				
			// analyze a range of posts
			} else if (args[0].equals("range")) {
				
				try{
					startIndex = Integer.parseInt(args[1]);
					numRowsToSelect = Integer.parseInt(args[2]);
					parsingMode = 1;
			    }catch(NumberFormatException e){
			    	System.err.println("Wrong Arguments - Exiting now");
			        System.exit(1);
			    }
				
			} else if (args[0].equals("all")) {
				try{
					parsingMode = 3;
				}catch(NumberFormatException e){
			    	System.err.println("Wrong Arguments - Exiting now");
			        System.exit(1);
			    }
			} else {
				
				System.err.println(" - Wrong arguments - ");
		    	System.err.println("   start with params:");
		    	System.err.println("   java -jar jarname single postID");
		    	System.err.println("   OR");
		    	System.err.println("   java -jar jarname range startIndex numRowsToSelect");
		        System.exit(1);
		        
			}
			
			// start a new thread
			StackToBaker bakerThread = new StackToBaker();
			
			
			long startTime = System.nanoTime();
			
			// start the extraction
			try {
				bakerThread.startExtraction();
			} catch (NullPointerException | ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			
			long duration = (System.nanoTime() - startTime)/1000000;  //divide by 1000000 to get milliseconds.
			
			long second = (duration / 1000) % 60;
			long minute = (duration / (1000 * 60)) % 60;
			long hour = (duration / (1000 * 60 * 60)) % 24;

			String time = String.format("%02d:%02d:%02d:%d", hour, minute, second, (duration% 1000));
			
			System.out.println("Analysis took " + time);
			
			// all done, exit the tool
			System.out.println("\n--- DONE ---");
			System.exit(0);
		}
	}

	/**
	 * gets a single post from the database
	 * @param pID the post-ID
	 * @return a StackOverflowPost-object
	 */
	public StackOverflowPost getSinglePostFromDb(int pID) {

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String question_query = "SELECT Id, Body, Tags, Title, PostTypeId, CreationDate, AcceptedAnswerId FROM filtered_posts WHERE id='"+pID+"'";
			
			ResultSet posts = st.executeQuery(question_query);
			
			StackOverflowPost post = null;
			
			while (posts.next())
			{
				post = new StackOverflowPost(posts.getInt("Id"), posts.getString("Body"),posts.getString("Tags"), posts.getString("Title"), posts.getInt("PostTypeId"), posts.getDate("CreationDate").toString(), posts.getInt("AcceptedAnswerId"));
			}
			posts.close(); //save heap-space
			conn.close();

			return post;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		//TODO: catch if null
		return null;

	}

	/**
	 * returns a list with Questions
	 * @param startIdx the start-index for the result of the query
	 * @param numRows the number of rows, which should be read
	 * @return List of StackOverflowPost-objects
	 */
	public ArrayList<StackOverflowPost> getQuestionsFromDb(int startIdx, int numRows) {
	
		ArrayList<StackOverflowPost> postList = new ArrayList<StackOverflowPost>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();
			
			// lower-limit: start row
			// upper-limit: num of rows to be selected
			
			numRows = Math.max(1, numRows); // dirty hotfix, because the numRows are sometimes at -99899999
			
			// get the questions
			String question_query = "SELECT Id, Body, Tags, Title, PostTypeId, CreationDate, AcceptedAnswerId FROM "
					+ "filtered_posts WHERE parentID IS NULL AND "
					+ "Id NOT IN (SELECT postID FROM post2api) "
					+ "LIMIT " + startIdx + ","+numRows+";";
			
			//System.out.println(question_query);
			
			ResultSet posts = st.executeQuery(question_query);
			
			//iterates through the resultset and adds each post to our list
			while (posts.next()) {
				StackOverflowPost p = new StackOverflowPost(posts.getInt("Id"), posts.getString("Body"), posts.getString("Tags"), posts.getString("Title"), posts.getInt("PostTypeId"), posts.getDate("CreationDate").toString(), posts.getInt("AcceptedAnswerId"));
				
				postList.add(p);
			}

			posts.close(); //save heap-space
			conn.close();

			return postList;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}
	
	/**
	 * extracts all codeBlocks from a posts
	 * code blocks are detected with the <code>-tag
	 * @param body  the body of the post
	 * @return a string with all blocks of code
	 */
	public String extractCodeFromPost(String body) {

		String codeOfBody = "";

		Pattern pattern = Pattern.compile("<code>(.+?)</code>");
		Matcher matcher = pattern.matcher(body);
		
		while (matcher.find()) {

			String code = matcher.group(1);
			if (code.contains(" ") || code.contains("\\.")) {
				codeOfBody += code + "\n";
				codeOfBody = codeOfBody.replace("&#xA;", "\n");
				codeOfBody = codeOfBody.replace("&gt;", ">");
				codeOfBody = codeOfBody.replace("&lt;", "<");
			}

		}
		
		return codeOfBody;
	}
	
	/**
	 * gets the JSON from the baker with all API-versions
	 * @param codeSnippet a code-snippet
	 * @return JSON-String
	 * @throws NullPointerException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	private String getJSONfromBaker(String codeSnippet) throws NullPointerException, ClassNotFoundException, IOException {
		
		String json = "";
		try {
			if(showOutput) {
				System.out.println("#> Starting baker");
			}
			json = JavaBaker.startBakerExternBody(codeSnippet);
			
			System.out.println(json);
			
			if(showOutput) {
				System.out.println("#> Baker finished");
			}
			json = json.replaceAll("<br>", "");
		} catch (IllegalArgumentException e) {
			System.out.println("catched IllegalArgumentException --> SKIP");
			json = "";
		} catch (NullPointerException e) {
			System.out.println("catched NullPointerException --> SKIP");
		}
		
		return json;
		
	}
	
	public void queryGraphDatabase() {
		for (String f : fqn) {
			List<String> versions = getVersions(f.replaceAll("\\[\"|\"\\]", ""));
		}
	}
	
	public List<String> getVersions(String fqn) {
		
		try {
			String cypher = "MATCH n WHERE n.fullName = {fqn} RETURN collect(distinct n.version)";
			
			Client client = Client.create();
			
			
			JSONObject tempJSON = new JSONObject();
			tempJSON.put("fqn",fqn);
			
			JSONObject json = new JSONObject();
			json.put("query", cypher);
			json.put("params", tempJSON);
			
			
			WebResource resource = client.resource("http://localhost:7474/db/data/cypher");
			ClientResponse response = resource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON)
					.entity(json.toString().replaceAll("[0-9]*[.][0-9]*$", "")).post(ClientResponse.class);
			String jsonString = response.getEntity(String.class);
			
			response.close();
			
			JSONParser parser = new JSONParser();
			
			
			JSONObject jsonObj = (JSONObject) parser.parse(jsonString);
			
			
			((JSONObject) jsonObj.get("data")).get(0).toString();
			
		
		} catch (org.json.simple.parser.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public List<String> getFullQualifiedNamesList(String snippetContent) {
		return null;
	}
	
	/**
	 * validates a given JSON
	 * @param json
	 * @return true - if JSON is valid | false - if json is empty/invalid
	 */
	private boolean validateJSON(String json) {
		if(!json.equals("") && !json.equals(emptyJSON)) {
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * Analyzes the returned JSON and detects the min/max version
	 * @param pID the postID
	 * @param json a JSON-String (from baker)
	 * @return Pair of min/max API-Level
	 */
	private Pair<Integer,Integer> parseJSON(int pID, String json, int postType) {
		Object obj = JSONValue.parse(json);
		
		
		JSONObject jsonObject = (JSONObject) obj;

		Object o = jsonObject.get("api_elements");
		
		Pair<Integer,Integer> api = new Pair<Integer, Integer>(99, -1);
		
		if (o instanceof JSONObject) {
			JSONObject oo = (JSONObject) jsonObject.get("api_elements");
			api = handleJSON(pID, oo, postType);
			
		} else if (o instanceof JSONArray) {
			JSONArray array = (JSONArray) jsonObject.get("api_elements");
			

			
			// -----------------------------
			for (Iterator<?> iter = array.iterator(); iter.hasNext();) {
				JSONObject oo = (JSONObject) iter.next();
				fqn.add(oo.get("elements").toString());
			}
			// -----------------------------
			
			
			
			for (Iterator<?> iter = array.iterator(); iter.hasNext();) {
				JSONObject oo = (JSONObject) iter.next();
				Pair<Integer,Integer> newApi = handleJSON(pID, oo, postType);

				// TODO: not perfect, see postID 937313
				
				// if any of the returnen values is distinct -> update
				int range = newApi.getRight() - newApi.getLeft();
				
				// if there is already a distinct api found
				if((api.getRight() - api.getLeft()) == 0) {
					
					// if the newly found is also distinct
					if(range == 0) {
						
						if(api.getLeft() > newApi.getLeft()) {
							api.setLeft(newApi.getLeft());
						}
						
						if(api.getRight() < newApi.getRight()) {
							api.setRight(newApi.getRight());
						}
					}
					
				} else {
					// no previous distinct values
					
					// new distinct values
					if(range == 0) {
						api.setLeft(newApi.getLeft());
						api.setRight(newApi.getRight());
					} else {
						// new min < current min
						if(api.getLeft() > newApi.getLeft()) {
							api.setLeft(newApi.getLeft());
						}
						
						// new max > current max
						if(api.getRight() < newApi.getRight()) {
							api.setRight(newApi.getRight());
						}
					}
				}
			}
			
		}
		
		return api;
		
	}
	
	/**
	 * analyzes a JSON-object and stores it into the database
	 * @param pID
	 * @param oo
	 */
	private Pair<Integer,Integer> handleJSON(int pID, JSONObject oo, int postType) {
		JSONArray elements = (JSONArray) oo.get("elements");
		
		Integer precision = new Integer(oo.get("precision").toString());
		String line_number = oo.get("line_number").toString();
		
		for (Object element : elements) {
			
			String fqn = element.toString();
			if (fqn.startsWith("\"\"")) {
				fqn = fqn.substring(2, fqn.length() - 2);
			}
			
			Pair<Integer,Integer> apiLevel = new Pair<Integer,Integer>(99, -1);
			
			// found a distinct API-Version
			// min/max from neo4j
			int min = Integer.parseInt(oo.get("min_version").toString());
			int max = Integer.parseInt(oo.get("max_version").toString());
			
			apiLevel = new Pair<Integer, Integer>(min, max);
			
			//storeElement2Posts(pID, precision, fqn, oo.get("name").toString(), line_number, oo.get("character").toString(), postType, min, max);
			
			if(showOutput)
				System.out.println("#> Found API-Level in Baker: " + min + " - " + max);
			
			
			return apiLevel;
		}
		
		
		// TODO nullcheck
		return null;
	}
	
	/**
    * detects the API-level from various elements of the post
    * @param pID the postID
    * @return List of Integers with API-levels
    */
	private void detectAPILevel(StackOverflowPost p) {
			
		try {
			int min = p.getMinAPI();
			int max = p.getMaxAPI();

			/* versionnumber in...
			 * 
			 * 			 ID
			 * tags:   6749261
			 * body:   2583150
			 * title:  8989622
			 * 
			 */
			
			// --- 1. search in tags ---
			
			ArrayList<String> tags = new ArrayList<String>();
			
			String tagStr = p.getTags();
			if(tagStr != null) { //no tags?
				tags.addAll(Arrays.asList(tagStr.substring(1, tagStr.length() -1).split("><")));
			}
			
			
			
			// if the post is a question and there are tags given
			if(!tags.isEmpty() && p.getType() == 1) {
				for(String s : tags) {
					if(keywordsToVersionMap.containsKey(s)){
						
						if(showOutput)
							System.out.println("#> Found match in tag: " + s);
						
						ArrayList<Integer> curElemVersions = keywordsToVersionMap.get(s);
						if(keywordsToVersionMap.get(s).size() > 1){
							if(min > curElemVersions.get(0)){
								min = curElemVersions.get(0);
								
								p.setFoundMinIn(FoundIn.POST_TAGS.value);
								
								if(showOutput)
									System.out.printf("#> Found new [min] in tags: " + min);
							}
							if(max < curElemVersions.get(curElemVersions.size()-1)){
								max = curElemVersions.get(curElemVersions.size()-1);

								p.setFoundMaxIn(FoundIn.POST_TAGS.value);
								
								if(showOutput)
									System.out.printf("#> Found new [max] in tags: " + max);
							}
						} else {
							if(min > curElemVersions.get(0)){
								min = curElemVersions.get(0);
								
								p.setFoundMinIn(FoundIn.POST_TAGS.value);
								
								if(showOutput)
									System.out.printf("#> Found new [min] in tags: " + min);
							}
							if(max < curElemVersions.get(0)){
								max = curElemVersions.get(0);
								
								p.setFoundMaxIn(FoundIn.POST_TAGS.value);
								
								if(showOutput)
									System.out.printf("#> Found new [max] in tags: " + max);
							}
						}
					}
				}
			}
			
			
			// --- 2. search in title of question ---
			if(p.getType() == 1) {
				for(String s : keywordsToVersionMap.keySet()){
					if(p.getTitle().matches("\b"+s.toLowerCase()+"\b")) {
						
						if(showOutput)
							System.out.println("#>> Found match in title: " + s);
						ArrayList<Integer> curElemVersions = keywordsToVersionMap.get(s);
						if(curElemVersions.size() > 1){
							if(min > curElemVersions.get(0)){
								min = curElemVersions.get(0);
								
								p.setFoundMinIn(FoundIn.POST_TITLE.value);
								
								if(showOutput)
									System.out.printf("#> Found new [min] in title: " + min);
							}
							if(max < curElemVersions.get(curElemVersions.size()-1)){
								max = curElemVersions.get(curElemVersions.size()-1);
								
								p.setFoundMaxIn(FoundIn.POST_TITLE.value);
								
								if(showOutput)
									System.out.printf("#> Found new [max] in title: " + max);
							}
						} else {
							if(min > curElemVersions.get(0)){
								min = curElemVersions.get(0);
								
								p.setFoundMinIn(FoundIn.POST_TITLE.value);
								
								if(showOutput)
									System.out.printf("#> Found new [min] in title: " + min);
							}
							if(max < curElemVersions.get(0)){
								max = curElemVersions.get(0);
								
								p.setFoundMaxIn(FoundIn.POST_TITLE.value);
								
								if(showOutput)
									System.out.printf("#> Found new [max] in title: " + max);
							}
						}
					}
				}
			}
			
			// --- 3. search in body ---
			
			//delete the code inside the question
			String body = p.getBody();
			
			body = body.replaceAll("(<code>).*?(</code>)", " ");
			
			//delete all tags
			body = body.replaceAll("<[a-zA-Z][a-zA-Z0-9= \":/.-]*>", " "); //tag-beginning
			body = body.replaceAll("(</[a-zA-Z]+>)", " "); //tag-end
			
			//delete special chars (line-break etc)
			body = body.replaceAll("(&#[a-zA-Z]+;)", " ");
			
			//delete multiple whitespaces and toLowerCase
			body = body.replaceAll("([ ])+", " ").toLowerCase();
			
			for(String s : keywordsToVersionMap.keySet()){
				if(body.matches("\b"+s.toLowerCase()+"\b")) {
					
					if(showOutput)
						System.out.println("#>> Found match in body: " + s);
					
					ArrayList<Integer> curElemVersions = keywordsToVersionMap.get(s);
					if(curElemVersions.size() > 1){
						if(min > curElemVersions.get(0)){
							min = curElemVersions.get(0);
							
							// if post is question
							if(p.getType() == 1){
								p.setFoundMinIn(FoundIn.POST_BODY.value);
							} else {
								// if post is (accepted) answer
								p.setFoundMinIn(FoundIn.ANSWER_BODY.value);
							}
							
							if(showOutput)
								System.out.printf("#> Found new [min] in body: " + min);
						}
						if(max < curElemVersions.get(curElemVersions.size()-1)){
							max = curElemVersions.get(curElemVersions.size()-1);
							
							// if post is question
							if(p.getType() == 1){
								p.setFoundMaxIn(FoundIn.POST_BODY.value);
							} else {
								// if post is (accepted) answer
								p.setFoundMaxIn(FoundIn.ANSWER_BODY.value);
							}
							
							if(showOutput)
								System.out.printf("#> Found new [max] in body: " + max);
						}
					} else {
						if(min > curElemVersions.get(0)){
							min = curElemVersions.get(0);
							
							// if post is question
							if(p.getType() == 1){
								p.setFoundMinIn(FoundIn.POST_BODY.value);
							} else {
								// if post is (accepted) answer
								p.setFoundMinIn(FoundIn.ANSWER_BODY.value);
							}
							
							if(showOutput)
								System.out.printf("#> Found new [min] in body: " + min);
						}
						if(max < curElemVersions.get(0)){
							max = curElemVersions.get(0);
							
							// if post is question
							if(p.getType() == 1){
								p.setFoundMaxIn(FoundIn.POST_BODY.value);
							} else {
								// if post is (accepted) answer
								p.setFoundMaxIn(FoundIn.ANSWER_BODY.value);
							}
							
							if(showOutput)
								System.out.printf("#> Found new [max] in body: " + max);
						}
					}
				}
			}
			
			//matches android x.x or android x.x.x
			Pattern pattern = Pattern.compile("(android [.0-9]{3}([.0-9]{2})?)");
			Matcher m = pattern.matcher(body);
			
			if(m.find()) {
				for (int i = 0; i < m.groupCount(); i++) {
					if(keywordsToVersionMap.containsKey(m.group(i))){
						
						if(showOutput)
							System.out.println("#>> Found match in body: " + m.group(i));
						
						ArrayList<Integer> curElemVersions = keywordsToVersionMap.get(m.group(i));
						if(curElemVersions.size() > 1){
							if(min > curElemVersions.get(0)){
								min = curElemVersions.get(0);
								
								// if post is question
								if(p.getType() == 1){
									p.setFoundMinIn(FoundIn.POST_BODY.value);
								} else {
									// if post is (accepted) answer
									p.setFoundMinIn(FoundIn.ANSWER_BODY.value);
								}
								
								if(showOutput)
									System.out.printf("#> Found new [min] in body: " + min);
							}
							if(max < curElemVersions.get(curElemVersions.size()-1)){
								max = curElemVersions.get(curElemVersions.size()-1);
								
								// if post is question
								if(p.getType() == 1){
									p.setFoundMaxIn(FoundIn.POST_BODY.value);
								} else {
									// if post is (accepted) answer
									p.setFoundMaxIn(FoundIn.ANSWER_BODY.value);
								}
								
								if(showOutput)
									System.out.printf("#> Found new [max] in body: " + max);
							}
						} else {
							if(min > curElemVersions.get(0)){
								min = curElemVersions.get(0);
								
								// if post is question
								if(p.getType() == 1){
									p.setFoundMinIn(FoundIn.POST_BODY.value);
								} else {
									// if post is (accepted) answer
									p.setFoundMinIn(FoundIn.ANSWER_BODY.value);
								}
								
								if(showOutput)
									System.out.printf("#> Found new [min] in body: " + min);
							}
							if(max < curElemVersions.get(0)){
								max = curElemVersions.get(0);
								
								// if post is question
								if(p.getType() == 1){
									p.setFoundMaxIn(FoundIn.POST_BODY.value);
								} else {
									// if post is (accepted) answer
									p.setFoundMaxIn(FoundIn.ANSWER_BODY.value);
								}
								
								if(showOutput)
									System.out.printf("#> Found new [max] in body: " + max);
							}
						}
					}
	            }
			}
			
			// --- 4. check the date of the question ---
			if(p.getCreationDate() != null && !p.getCreationDate().equals("") && p.getType() == 1) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				
				Date date = dateFormat.parse(p.getCreationDate().trim());
			    long creationDateUNIX = (long) date.getTime()/1000;
			    
			    boolean found = false;
			    
			    int i = 1;
			    
			    for(Integer d : androidVersionDates) {
			    	if(d.intValue() < creationDateUNIX) {
			    		found = true;
			    		if(i > max) {
			    			max = i;
			    			p.setFoundMaxIn(FoundIn.POST_DATE.value);
			    		}
			    	}
			    	i++;
			    }
			    
			    if(found) {
			    	if(showOutput)
			    		System.out.println("#> Found new [max] in date: " + max);
			    	p.setMaxAPI(max);
			    } else {
			    	max = totalMinVersion;
			    }
			}
			
			if(min < p.getMinAPI()) {
				p.setMinAPI(min);
			}
			
			if(max > p.getMaxAPI()) {
				p.setMaxAPI(max);
			}
			
			
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Starts the analysis process for a given post
	 * @param p the post
	 */
	private void analyzePost(StackOverflowPost p) {

		// 1. detect the API-Level in the title, body, tags and date
		
		if(showOutput)
			System.out.println("> Analyzing date, title, text and tags of post");
		
		detectAPILevel(p);
		
		// if there was no distinct api found
		if(p.getMinAPI() != p.getMaxAPI()) {
			if(showOutput)
				System.out.println("> Extracting code");
			
			// extract the code-blocks from the body of the post
			String code = extractCodeFromPost(p.getBody());
			
			// if there is a codeblock
			if(!code.trim().equals("")){
				
				if(showOutput)
					System.out.println("> Query baker");
				
				String json = "";
				
				try {
					// call the bakertool and retreive the JSON with API-Versions
					json = getJSONfromBaker(code);
				} catch (NullPointerException | ClassNotFoundException | IOException e) {
					e.printStackTrace();
				}
				
				//System.out.println(json);
				
				// check, if the returned JSON is valid
				if(validateJSON(json) == true) {
					if(showOutput)
						System.out.println("> Analyzing results from Baker");
					
					// query the JSON and find min/max API-Version from the JSON
					Pair<Integer, Integer> apiLevel = parseJSON(p.getId(), json, p.getType());
					
					queryGraphDatabase();
					
					// if the baker-tool found some API-Versions
					if(apiLevel.getLeft() < 99 || apiLevel.getRight() > -1) {
						
						if(apiLevel.getLeft() < 99)
							// if the found min is bigger then the current min
							if(apiLevel.getLeft() < p.getMinAPI()) {
								p.setMinAPI(apiLevel.getLeft());
								
								// if the post is a querstion
								if(p.getType() == 1) {
									p.setFoundMinIn(FoundIn.POST_CODE.value); //found in Baker (question)
								} else {
									p.setFoundMinIn(FoundIn.ANSWER_CODE.value); //found in Baker (answer)
								}
							}
						if(apiLevel.getRight() > -1) {
							// if the found max is smaller then the current max
							if(apiLevel.getRight() < p.getMaxAPI()) {
								p.setMaxAPI(apiLevel.getRight());
								
								// if the post is a querstion
								if(p.getType() == 1) {
									p.setFoundMaxIn(FoundIn.POST_CODE.value); //found in Baker (question)
								} else {
									p.setFoundMaxIn(FoundIn.ANSWER_CODE.value); //found in Baker (answer)
								}
							}
						}
						
						
					} else {
						if(showOutput)
							System.out.println("#> No API found with Baker");
					}
					
				} else {
					if(showOutput)
						System.out.println("#> INVALID JSON FOUND");
				}
				
			} else {
				if(showOutput)
					System.out.println("#> NO CODE FOUND");
			}
		}
		
		

		// no Minimum found
		if(p.getMinAPI() == 99) {
			p.setMinAPI(totalMinVersion);
		}
		
		// no Maximum found
		if(p.getMaxAPI() == -1) {
			p.setMaxAPI(totalMaxVersion);
		}
		
	}

	/**
	 * stores an element into the database
	 * @param id_post
	 * @param precision
	 * @param fqn
	 * @param name
	 * @param line
	 * @param character
	 * @param type
	 * @param postTypeId
	 * @param minV
	 * @param maxV
	 */
	private void storeElement2Posts(int id_post, int precision, String fqn, String name, String line, String character, int postTypeId, int minV, int maxV) {
			
		PreparedStatement insertElementsStatement = null;
		// insert
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			conn.createStatement();

			insertElementsStatement = conn.prepareStatement("INSERT INTO elements2posts (postid, _precision, fqn, name, line, _char, postTypeId, min_version, max_version) VALUES (" + id_post + ", " + precision + ", '" + fqn + "', '" + name + "', '" + line + "', '" + character +"', '" + postTypeId +"', '" + minV +"', '" + maxV +"');");
			
			insertElementsStatement.executeUpdate();

			conn.close();
			insertElementsStatement.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * 
	 * @param postID
	 * @param result
	 */
	private void storePostToDB(StackOverflowPost p) {
		if(showOutput)
			System.out.println("> Storing results into database");
		
		PreparedStatement insertPostStatement = null;
		// insert
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			
			
			conn.createStatement();

			insertPostStatement = conn.prepareStatement("INSERT INTO post2api (postid, foundMinIn, foundMaxIn, minAPI, maxAPI) VALUES (" + p.getId() + ", " + p.getFoundMinIn() + "," + p.getFoundMaxIn() + ", " + p.getMinAPI() + ", '" + p.getMaxAPI() + "');");
			
			insertPostStatement.executeUpdate();
			insertPostStatement.close();

			conn.close();
			

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}catch (MySQLIntegrityConstraintViolationException e) {
			if(showOutput)
				System.err.print("#> Post was already in Database <#");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * loads the previously found result for a post, if there is any
	 * @param postID the post id
	 * @return the found result
	 */
	private PostResult loadResultFromDB(int postID) {
		
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			String question_query = "SELECT * FROM post2api WHERE postID='"+postID+"'";
			
			ResultSet posts = st.executeQuery(question_query);
			
			if(posts.next()) {
				return new PostResult(posts.getInt("postID"), posts.getInt("foundMinIn"), posts.getInt("foundMaxIn"), posts.getInt("minAPI"), posts.getInt("maxAPI"));
			}

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		//TODO: catch if null
		return null;
	}
	
	/**
	 * starts the extraction-thread
	 * @throws NullPointerException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("static-access")
	public void startExtraction() throws NullPointerException, ClassNotFoundException, IOException {
		
		// parse a single post
		if(parsingMode == 0) {
			
			if(showOutput)
				System.out.println("------------------------------------------------------------");
			
			System.out.println("[ Analyzing Post #"+ passedPostID +" ]");
			
			PostResult r = loadResultFromDB(passedPostID);
			
			if(r == null) {
				System.out.println("> Post was already analyzed");
				System.out.println("\n> Data for Post #" + passedPostID + ":");
				System.out.println("> minAPI:\t" + r.getMinAPI());
				System.out.println("> maxAPI:\t" + r.getMaxAPI());
				System.out.println("> found Min in:\t" + r.getFoundMinIn());
				System.out.println("> found Max in:\t" + r.getFoundMaxIn());
			} else {
				StackOverflowPost post = getSinglePostFromDb(passedPostID);
				
				analyzePost(post);
				
				// if the post has an accepted answer and there was no distinct API found yet
				if(post.getAcceptedAnswerId() > 0) {
					if(post.getMinAPI() == post.getMaxAPI()){
						if(showOutput)
							System.out.println("> Post has an accepted answer but a distinct API was already found");
					} else {
						if(showOutput)
							System.out.println("> Checking Accepted Answer");
						StackOverflowPost ans = getSinglePostFromDb(post.getAcceptedAnswerId());
					
						analyzePost(ans);
						
						// if accepted answer has new maximum
						if(ans.getMaxAPI() < post.getMaxAPI()) {
							post.setMaxAPI(ans.getMaxAPI());
							post.setFoundMaxIn(ans.getFoundMaxIn());
						}
						
						// if accepted answer has new maximum
						if(ans.getMinAPI() > post.getMinAPI()) {
							post.setMinAPI(ans.getMinAPI());
							post.setFoundMinIn(ans.getFoundMinIn());
						}
						
						// if the accepted answer collides with the found api-versions of the question
						if(post.getMinAPI() > post.getMaxAPI()) {
							post.setMinAPI(post.getMaxAPI());
						}
					}
				} else {
					if(showOutput)
						System.out.println("> Post has no accepted answer");
				}

				//storePostToDB(post);
				

				if(post.getMinAPI() == post.getMaxAPI()) {
					System.out.println("[ Post #" + post.getId() + " is probably about API-Version " + post.getMinAPI() +" ]");
				} else {
					System.out.println("[ Post #" + post.getId() + " is probably about API-Version " + post.getMinAPI() + " to " + post.getMaxAPI() + " ]");
				}
				
			}
			
		// parse a range of posts
		} else if(parsingMode == 1) {
			
			int totalNum = this.numRowsToSelect;	// the total number of posts to analyze
			int startIdx = this.startIndex;			// the startindex
				
			if(showOutput)
				System.out.println("> Getting "+totalNum+" posts from DB");
			
			ArrayList<StackOverflowPost> posts = getQuestionsFromDb(startIdx, totalNum);
			
			if(showOutput)
				System.out.println("> Got "+totalNum+" posts from DB");
			
			for(StackOverflowPost p : posts) {
				
				if(showOutput)
					System.out.println("------------------------------------------------------------");
				
				System.out.println("[ Analyzing Post #"+ p.getId() +" ]");
				
				PostResult r = loadResultFromDB(p.getId());
				
				if( r != null) {
					System.out.println("> Post was already analyzed");
					System.out.println("\n> Data for Post #" + p.getId() + ":");
					System.out.println("> minAPI:\t" + r.getMinAPI());
					System.out.println("> maxAPI:\t" + r.getMaxAPI());
					System.out.println("> found Min in:\t" + r.getFoundMinIn());
					System.out.println("> found Max in:\t" + r.getFoundMaxIn());
				} else {
					
					analyzePost(p);
					
					// if the post has an accepted answer and there was no distinct API found yet
					if(p.getAcceptedAnswerId() > 0) {
						if(p.getMinAPI() == p.getMaxAPI()){
							if(showOutput)
								System.out.println("> Post has an accepted answer but a distinct API was already found");
						} else {
							System.out.println("> Checking Accepted Answer");
							StackOverflowPost ans = getSinglePostFromDb(p.getAcceptedAnswerId());
						
							analyzePost(ans);
							
							// if accepted answer has new maximum
							if(ans.getMaxAPI() < p.getMaxAPI()) {
								p.setMaxAPI(ans.getMaxAPI());
								p.setFoundMaxIn(ans.getFoundMaxIn());
							}
							
							// if accepted answer has new maximum
							if(ans.getMinAPI() > p.getMinAPI()) {
								p.setMinAPI(ans.getMinAPI());
								p.setFoundMinIn(ans.getFoundMinIn());
							}
							
							// if the accepted answer collides with the found api-versions of the question
							if(p.getMinAPI() > p.getMaxAPI()) {
								p.setMinAPI(p.getMaxAPI());
							}
						}
					} else {
						if(showOutput)
							System.out.println("> Post has no accepted answer");
					}

					storePostToDB(p);
					

					if(p.getMinAPI() == p.getMaxAPI()) {
						System.out.println("[ Post #" + p.getId() + " is probably about API-Version " + p.getMinAPI() +" ]");
					} else {
						System.out.println("[ Post #" + p.getId() + " is probably about API-Version " + p.getMinAPI() + " to " + p.getMaxAPI() + " ]");
					}
				}
			}
			
		// analyze all posts
		} else if(parsingMode == 3) {
			
			ArrayList<StackOverflowPost> posts = getQuestionsFromDb(0, 10);
			
			// while there are still posts to be processed
			while(posts.size() > 0) {
				
				for(StackOverflowPost p : posts) {
					
					if(showOutput)
						System.out.println("------------------------------------------------------------");
					
					System.out.println("[ Analyzing Post #"+ p.getId() +" ]");
					
					PostResult r = loadResultFromDB(p.getId());
					
					if(r != null) {
						if(showOutput) {
							System.out.println("> Post was already analyzed");
							System.out.println("\n> Data for Post #" + passedPostID + ":");
							System.out.println("> minAPI:\t" + r.getMinAPI());
							System.out.println("> maxAPI:\t" + r.getMaxAPI());
							System.out.println("> found Min in:\t" + r.getFoundMinIn());
							System.out.println("> found Max in:\t" + r.getFoundMaxIn());
						}
					} else {
					
						analyzePost(p);
						
						// if the post has an accepted answer and there was no distinct API found yet
						if(p.getAcceptedAnswerId() > 0) {
							if(p.getMinAPI() == p.getMaxAPI()){
								if(showOutput)
									System.out.println("> Post has an accepted answer but a distinct API was already found");
							} else {
								System.out.println("> Checking Accepted Answer");
								StackOverflowPost ans = getSinglePostFromDb(p.getAcceptedAnswerId());
							
								analyzePost(ans);
								
								// if accepted answer has new maximum
								if(ans.getMaxAPI() < p.getMaxAPI()) {
									p.setMaxAPI(ans.getMaxAPI());
									p.setFoundMaxIn(ans.getFoundMaxIn());
								}
								
								// if accepted answer has new maximum
								if(ans.getMinAPI() > p.getMinAPI()) {
									p.setMinAPI(ans.getMinAPI());
									p.setFoundMinIn(ans.getFoundMinIn());
								}
								
								// if the accepted answer collides with the found api-versions of the question
								if(p.getMinAPI() > p.getMaxAPI()) {
									p.setMinAPI(p.getMaxAPI());
								}
							
							}
						} else {
							if(showOutput)
								System.out.println("> Post has no accepted answer");
						}

						storePostToDB(p);
						

						if(p.getMinAPI() == p.getMaxAPI()) {
							System.out.println("[ Post #" + p.getId() + " is probably about API-Version " + p.getMinAPI() +" ]");
						} else {
							System.out.println("[ Post #" + p.getId() + " is probably about API-Version " + p.getMinAPI() + " to " + p.getMaxAPI() + " ]");
						}
				
				
					}
				}
				
				// load next 10 posts
				posts = getQuestionsFromDb(0, 10);
				
			}
		}
		
		if(showOutput)
			System.out.println("------------------------------------------------------------");
		
	}
	
	
	
	
	
	public List<String> extractCode(String body) {
		
		List<String> returnList = new ArrayList<>();

		String codeOfBody = "";

		Pattern pattern = Pattern.compile("<code>(.+?)</code>");
		Matcher matcher = pattern.matcher(body);
		while (matcher.find()) {

			String code = matcher.group(1);
			if (code.contains(" ") || code.contains("\\.")) {
				codeOfBody += code + "\n";
				codeOfBody = codeOfBody.replace("&#xA;", "\n");
				codeOfBody = codeOfBody.replace("&gt;", ">");
				codeOfBody = codeOfBody.replace("&lt;", "<");
				returnList.add(codeOfBody);
				codeOfBody = "";
			}

		}
		
		codeOfBody = removeExceptions(codeOfBody);
		
		return returnList;

	}


	/**
	 * gets all answers to a post
	 * @param parentID the ID of the parent-post
	 * @return List of StackOverflowPost-objects
	 */
	public List<StackOverflowPost> getAnswersFromDb(int parentID) {

		List<StackOverflowPost> postList = new ArrayList<>();

		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();
			
//			String answeres_query = "SELECT Id, Title, CreationDate, Body, Tags, PostTypeId FROM "
//					+ "filtered_posts WHERE "
//					+ "PostTypeId='2' AND "
//					+ "parentID = "+parentID+" "
//					+ "AND (Body LIKE '%<code>%</code>%' "
//					+ "AND Id NOT IN (SELECT DISTINCT postid FROM elements2posts )) ORDER BY Id";
			
			// removed the need for the body to contain code
			String answeres_query = "SELECT Id, Title, CreationDate, Body, Tags, PostTypeId, AcceptedAnswerId FROM "
					+ "filtered_posts WHERE "
					+ "PostTypeId='2' AND "
					+ "parentID = "+parentID+" "
					+ "AND Id NOT IN (SELECT DISTINCT postid FROM elements2posts ) ORDER BY Id";
			

			ResultSet posts = st.executeQuery(answeres_query);
			
			while (posts.next()) {

				StackOverflowPost post = new StackOverflowPost(posts.getInt("Id"), posts.getString("Body"), 
						posts.getString("Tags"), posts.getString("Title"), posts.getInt("PostTypeId"), posts.getDate("CreationDate").toString(), posts.getInt("AcceptedAnswerId"));
				
				postList.add(post);
			}
			
			st.close();  //save heap-space
			conn.close();
			System.gc(); //save heap-space

			return postList;

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return null;
		
	}

	/**
	 * removes exception from code-snippets
	 * @param code
	 * @return exception-free code-snippet
	 */
	private String removeExceptions(String code) {
		String[] lines = code.split("\n");
		StringBuffer sb = new StringBuffer();

		for (String line : lines) {

			Pattern pattern = Pattern.compile("(\\d?\\d?(-|:|\\.|\\s)?\\.?)+\\s*(ERROR|DEBUG|INFO)/.*");
			Matcher matcher = pattern.matcher(line);

			if (!matcher.matches()) {
				String regex = "((\\w+(\\.|\\/))+\\w+\\}:)?\\s(\\w+\\.)+\\w+Exception:.*(\\w+\\.)+\\w+\\(\\w*\\.\\w*:\\d+1\\)";

				line = line.replaceAll(regex, "");
				
				line += "\n";

				sb.append(line);
			}

		}
		
		return sb.toString();

	}

}
