package queue;

import java.util.Arrays;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;

public abstract class Queue {

	private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
	private Session session;
	
	private String activeMQServer = "tcp://avian:61616";
	
    private final String mysqlServerUrl = "jdbc:mysql://localhost:3306/";
    private final String mysqlDatabaseName = "so2016_12";
    private final String mysqlUsername = "root";
    private final String mysqlPassword = "";
    private final String mysqlDriver = "com.mysql.jdbc.Driver";
    
	private final int prefetchSize = 1;
	
	public boolean showOutput = true;

	public Queue() {
		
		// Create a ConnectionFactory
		this.connectionFactory = new ActiveMQConnectionFactory(activeMQServer);
		
		this.connectionFactory.setTrustedPackages(Arrays.asList(new String[]{"queue", "worker", "java.lang"}));
		
		try {
			
			// Create a Connection
			this.connection = this.connectionFactory.createConnection();
		
			this.connection.start();
			
			// Create a Session
			this.session = this.connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
	
		} catch (JMSException e) {
			e.printStackTrace();
		}
		
	}
	
	public abstract void run();
	
	public void shutDown() {
		try {
			this.connection.close();
			this.session.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public ActiveMQConnectionFactory getConnectionFactory() {
		return this.connectionFactory;
	}
	
	public Connection getConnection() {
		return connection;
	}

	public Session getSession() {
		return session;
	}
	
	public String getActiveMQServer() {
		return activeMQServer;
	}
	
	public String getMysqlServerUrl() {
		return mysqlServerUrl;
	}

	public String getMysqlDatabaseName() {
		return mysqlDatabaseName;
	}

	public String getMysqlUsername() {
		return mysqlUsername;
	}

	public String getMysqlPassword() {
		return mysqlPassword;
	}

	public String getMysqlDriver() {
		return mysqlDriver;
	}

    public int getPrefetchSize() {
		return prefetchSize;
	}
}
