package queue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;

import worker.BakerQuery;
import worker.StackOverflowPost;

public class AcceptedAnsBakerQueryQueue extends Queue implements Runnable, ExceptionListener {

	public AcceptedAnsBakerQueryQueue() {
		super();
	}
	
	public void run() {
		
		try {
			
			Random randomGenerator = new Random();
	 		int rndID = randomGenerator.nextInt(100000000);

		    // Create the destination (Topic or Queue)
		    Destination source = super.getSession().createQueue("AnalyzeAcceptedAnsCodeQueue?consumer.prefetchSize=" + super.getPrefetchSize());
		    
		    // Create the destination (Topic or Queue)
		    Destination destination = super.getSession().createQueue("ProcessedPostsQueue?consumer.prefetchSize=" + super.getPrefetchSize());

		    // Create a MessageProducer from the Session to the Topic or Queue
		    MessageProducer producer = super.getSession().createProducer(destination);
		    
		 
		    producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		
		    // Create a MessageConsumer from the Session to the Topic or Queue
		    MessageConsumer consumer = super.getSession().createConsumer(source);
		    
		    Message message = null;
            boolean running=true;
            
            while(running) {
                message = consumer.receive();
                if (message instanceof ObjectMessage && message != null) {
                    Serializable object = ((ObjectMessage) message).getObject();
                    if (object instanceof StackOverflowPost) {
                    	
                    	StackOverflowPost post = (StackOverflowPost)object;
                    	
                    	if(super.showOutput)
							System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [AcceptedAnsBakerQuery] Processing post #" + post.getPostID());
                    	
                    	try {
                    		
                    		long start = System.currentTimeMillis();
                    		new BakerQuery(post);
                    		long duration = System.currentTimeMillis() - start;
	                        
                    		
                    		if(super.showOutput)
    							System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [AcceptedAnsBakerQuery] " + post.getMinVersion() + " - " + post.getMaxVersion() + " :: DURATION " + duration + "ms");
	                        
	                        ObjectMessage msg = super.getSession().createObjectMessage(post);
	                        
                        	// send it to the next queue
	                        if(super.showOutput)
								System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [AcceptedAnsBakerQuery] " + post.getMinVersion() + " != " + post.getMaxVersion() + " --> sending Post to ProcessedPostsQueue");
	                        
                        	message.acknowledge();
                        	producer.send(msg);
	                        
	                        
                        	if(super.showOutput) {
    							System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [AcceptedAnsBakerQuery] Processed post #" + post.getPostID());
	                        	System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " -----------------------------------------------------------");
                        	}
                    	
                		} catch(com.sun.jersey.api.client.ClientHandlerException e) {
                         		
                         		// neo4j crashed
                         		e.printStackTrace();
                         		
                         		BufferedWriter bw = null;
                         		FileWriter fw = null;

                         		try {

                         			File file = new File("failedIDs_"+rndID+".txt");

                         			// if file doesnt exists, then create it
                         			if (!file.exists()) {
                         				file.createNewFile();
                         			}

                         			// true = append file
                         			fw = new FileWriter(file.getAbsoluteFile(), true);
                         			bw = new BufferedWriter(fw);

                         			bw.write(post.getPostID().toString() + "\n");

                         		} catch (IOException e2) {
                         			e2.printStackTrace();
                         		} finally {
                         			try {
                         				if (bw != null)
                         					bw.close();

                         				if (fw != null)
                         					fw.close();

                         			} catch (IOException ex) {
                         				ex.printStackTrace();
                         			}
                         		}
                         		
                         		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [AcceptedAnsBakerQuery] EXCEPTION THROWN - NEO4J IS DEAD");
                         		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " -----------------------------------------------------------");
                         		
                         		System.exit(1);
                         		
                         	} catch(Exception e) {
                         		
                         		BufferedWriter bw = null;
                         		FileWriter fw = null;

                         		try {

                         			File file = new File("failedIDs_"+rndID+".txt");

                         			// if file doesnt exists, then create it
                         			if (!file.exists()) {
                         				file.createNewFile();
                         			}

                         			// true = append file
                         			fw = new FileWriter(file.getAbsoluteFile(), true);
                         			bw = new BufferedWriter(fw);

                         			bw.write(post.getPostID().toString() + "\n");

                         		} catch (IOException e2) {
                         			e2.printStackTrace();
                         		} finally {
                         			try {
                         				if (bw != null)
                         					bw.close();

                         				if (fw != null)
                         					fw.close();

                         			} catch (IOException ex) {
                         				ex.printStackTrace();
                         			}
                         		
                         		}
                         		
                         		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [AcceptedAnsBakerQuery] EXCEPTION THROWN");
                         		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " -----------------------------------------------------------");
                         		
     	                    	e.printStackTrace();
     	                	}
                    }
                }
            }
            
		    producer.close();
		    
		} catch (Exception e) {
		    System.out.println("Caught: " + e);
		    e.printStackTrace();
		}
	}

	public synchronized void onException(JMSException ex) {
		System.out.println("JMS Exception occured.  Shutting down client.");
	}
}