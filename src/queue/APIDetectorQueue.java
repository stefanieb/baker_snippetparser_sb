package queue;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;


import worker.APIDetector;
import worker.StackOverflowPost;

public class APIDetectorQueue extends Queue implements Runnable, ExceptionListener {

	public APIDetectorQueue() {
		super();
	}
	
	public void run() {
	
		try {

		    // Source of the StackOverflow-Post Objects
		    Destination source = super.getSession().createQueue("PostObjectsQueue?consumer.prefetchSize=" + super.getPrefetchSize());
		    
		    // Destination for the StackOverflow-Post Objects, if there is no distinct API found
		    Destination destination = super.getSession().createQueue("APIDetectorQueue?consumer.prefetchSize=" + super.getPrefetchSize());

		    // Create a MessageProducer from the Session to the Topic or Queue
		    MessageProducer producer = super.getSession().createProducer(destination);
		    
		    // Destination for the StackOverflow-Post Objects, if there is a distinct API found
		    Destination finishedDestionation = super.getSession().createQueue("ProcessedPostsQueue?consumer.prefetchSize=" + super.getPrefetchSize());
		    
		    // Create a MessageProducer from the Session to the Topic or Queue
		    MessageProducer producerFinishedPost = super.getSession().createProducer(finishedDestionation);
		    
		    producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		
		    // Create a MessageConsumer from the Session to the Topic or Queue
		    MessageConsumer consumer = super.getSession().createConsumer(source);
		    
		    Message message = null;
            boolean running=true;
            
            while(running) {
                message = consumer.receive();
                if (message instanceof ObjectMessage && message != null) {
                    Serializable object = ((ObjectMessage) message).getObject();
                    if (object instanceof StackOverflowPost) {
                    	
                    	StackOverflowPost post = (StackOverflowPost)object;
                    	if(super.showOutput)
							System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [APIDetector] Processing post #" + post.getPostID());
                    	
                    	try {
	                		new APIDetector(post, 0);
	                    	
	                        message.acknowledge();
	                        
	                        ObjectMessage msg = super.getSession().createObjectMessage(post);
	                        
	                        // if a distinct version was found
	                        if(post.getMinVersion() == post.getMaxVersion()) {
	                        	// send it to the last queue
	                        	if(super.showOutput)
	                        		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [APIDetector] " + post.getMinVersion() + " == " + post.getMaxVersion() + " --> sending Post to ProcessedPostsQueue");
	                        	producerFinishedPost.send(msg);
	                        } else {
	                        	// send it to the next queue
	                        	if(super.showOutput)
	    							System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [APIDetector] " + post.getMinVersion() + " != " + post.getMaxVersion() + " --> sending Post to APIDetectorQueue");
	                        	producer.send(msg);
	                        }
	                    } catch(Exception e) {
	                		e.printStackTrace();
	                	}
                    }
                }
            }
		    
		    producer.close();
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
	}

	public synchronized void onException(JMSException ex) {
		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " JMS Exception occured.  Shutting down client.");
	}
}