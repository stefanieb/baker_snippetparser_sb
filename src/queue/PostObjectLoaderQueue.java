package queue;

import worker.StackOverflowPost;
import worker.PostLoader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;

import com.mysql.jdbc.Statement;
import com.mysql.jdbc.exceptions.MySQLNonTransientConnectionException;

public class PostObjectLoaderQueue extends Queue implements Runnable, ExceptionListener {
	
    public PostObjectLoaderQueue() {
		super();
	}
    
 	public void run() {
 		
 		
		try {
		    // Create the destination (Topic or Queue)
		    Destination destination = super.getSession().createQueue("PostObjectsQueue?consumer.prefetchSize=" + super.getPrefetchSize());
		    
		    // Create a MessageProducer from the Session to the Topic or Queue
		    MessageProducer producer = super.getSession().createProducer(destination);
		 
		    producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		    
		    
		    PostLoader pl = new PostLoader(super.getMysqlServerUrl(), super.getMysqlDatabaseName(), super.getMysqlUsername(), super.getMysqlPassword(), super.getMysqlDriver());
		    
		    BufferedReader br = null;
	        String line = "";
	        
//	        try {
//	
//	            br = new BufferedReader(new FileReader("metrics/testing_posts.csv"));
////	            br = new BufferedReader(new FileReader("metrics/benchmark_posts.csv"));
//	            
//	            
//	            while ((line = br.readLine()) != null) {
//	            	StackOverflowPost post = pl.loadPost(Integer.parseInt(line));
//	            	if(post.getPostID() > 0) {
//                    	System.out.println("[PostObjectLoader] Pushing post #" + post.getPostID() + " to the queue");
//						ObjectMessage msg = super.getSession().createObjectMessage(post);
//						producer.send(msg);
//	            	}
//	            }
//	
//	        } catch (FileNotFoundException e) {
//	            e.printStackTrace();
//	        } catch (IOException e) {
//	            e.printStackTrace();
//	        } finally {
//	            if (br != null) {
//	                try {
//	                    br.close();
//	                } catch (IOException e) {
//	                    e.printStackTrace();
//	                }
//	            }
//	        }
	        
	        int id = 40882357;
	        
	        System.out.println("processing post #" + id);
	        
	        StackOverflowPost post = pl.loadPost(id);
			
			if((int)post.getPostID() == -1 ) {
				System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " error loading post -> trying to load (" + post.getPostID() +") and got ID (" + post.getPostID().intValue() + ")");
			} else {
				if(super.showOutput)
					System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " Pushing post #"+ post.getPostID().intValue() +" to Queue");
				
				ObjectMessage msg = super.getSession().createObjectMessage(post);
				producer.send(msg);
			}
			
			System.exit(0);
		    
//		    try {
//				Class.forName(super.getMysqlDriver()).newInstance();
//				java.sql.Connection conn = (java.sql.Connection) DriverManager.getConnection(super.getMysqlServerUrl() + super.getMysqlDatabaseName(), super.getMysqlUsername(), super.getMysqlPassword());
//				java.sql.Statement st = conn.createStatement();
//
//				String question_query = "SELECT Id FROM filtered_posts WHERE PostTypeId = 1 AND Id IN (1711078, 2123743, 2470520, 2558079, 2926098, 3262781, 3285585, 3438276, 3551624, 3658088, 3670859, 3792612, 3797992, 3961125, 4068074, 4123941, 4131619, 4287280, 4474993, 4489173, 4535664, 4543349, 4820073, 4844499, 4887071, 4999913, 5069913, 5100071, 5268749, 5443556, 5616938, 5631826, 5692804, 5779680, 5808577, 5849628, 5888544, 5966396, 6084484, 6135664, 6138422, 6143714, 6342318, 6377221, 6419989, 6437360, 6508341, 6509166, 6522710, 6823017, 6976178, 7042390, 7048314, 7132751, 7320820, 7391816, 7497749, 7555574, 7672334, 7751898, 7821479, 8001921, 8008020, 8048259, 8089389, 8105174, 8128882, 8237550, 8247026, 8253817, 8394926, 8420168, 8525166, 8662453, 9108204, 9225673, 9324181, 9333538, 9344658, 9395827, 9693755, 9868336, 9886740, 9915545, 9921034, 9938907, 10012670, 10071119, 10087866, 10088144, 10172374, 10221118, 10259006, 10352268, 10510722, 10581183, 10789256, 10875864, 10975395, 10994631, 11087883, 11194182, 11242601, 11269559, 11294626, 11392629, 11506625, 11676534, 11748335, 11788242, 11796022, 11840381, 11851962, 11877901, 11933392, 11959592, 12068832, 12127831, 12141848, 12155002, 12247049, 12365297, 12507892, 12511140, 12519985, 12931088, 12963187, 13146991, 13273124, 13406015, 13451373, 13493670, 13583108, 13596847, 13672951, 13783424, 13797617, 14016732, 14078278, 14138871, 14182654, 14374034, 14399039, 14594289, 14749138, 14873847, 14875255, 14909993, 14923950, 15015040, 15158826, 15275498, 15287289, 15325951, 15354336, 15357405, 15400855, 15524717, 15525191, 15552076, 15571616, 15652591, 15748207, 15753275, 16189962, 16540550, 16572545, 16593084, 16708368, 16910402, 17055495, 17152981, 17181663, 17203945, 17313575, 17326862, 17363967, 17388902, 17508000, 17533480, 17638563, 17761337, 17786589, 17796235, 17895538, 17929402, 18019042, 18152183, 18195653, 18254417, 18301257, 18306172, 18306838, 18323213, 18473018, 18483360, 18577149, 18583009, 18736049, 18781501, 18840772, 19002998, 19046318, 19070149, 19241450, 19261474, 19263057, 19318002, 19432549, 19445744, 19507964, 19509292, 19525587, 19529968, 19653581, 19701535, 19800076, 19825288, 19891168, 19922393, 19943422, 19949420, 20172055, 20436825, 20436898, 20517361, 20520933, 20588326, 20664719, 20796223, 20829536, 20841781, 20896930, 20927810, 20935890, 20943799, 20969306, 21089161, 21146240, 21187480, 21202996, 21268645, 21305651, 21317468, 21371049, 21539055, 21718107, 21832954, 21839776, 21890164, 22161958, 22166645, 22196374, 22299047, 22417131, 22994983, 23016474, 23156317, 23166897, 23204809, 23296271, 23342655, 23462732, 23721943, 23752503, 23843055, 23902892, 23976044, 24013398, 24175984, 24474311, 24532894, 24563492, 24817466, 24859908, 24955618, 25038828, 25214104, 25281224, 25289645, 25328780, 25627548, 25738088, 25742408, 25759665, 25917327, 25934917, 25947364, 25996567, 26036117, 26045218, 26143218, 26164246, 26477423, 26480730, 26506302, 26779184, 27066291, 27221758, 27234448, 27322916, 27378714, 27471141, 27523008, 27599678, 27642624, 28072700, 28116629, 28119439, 28132826, 28209298, 28217041, 28243278, 28498998, 28524799, 28536336, 28778424, 28909840, 28930243, 29050469, 29072260, 29087675, 29094832, 29135636, 29372113, 29495793, 29501728, 29583536, 29639217, 29705280, 29867348, 29869039, 29965049, 30293532, 30435642, 30472031, 30477525, 30486753, 30490402, 30616946, 30676572, 30724003, 30778380, 30802523, 30924103, 30978114, 30986163, 31335317, 31438114, 31578707, 31664347, 31759619, 31913098, 31938595, 31946536, 32013174, 32053620, 32080687, 32105532, 32139191, 32167669, 32169593, 32175932, 32201564, 32306445, 32341662, 32419819, 32454891, 32467164, 32499302, 32520344, 32631442, 32683432, 32686893, 32723337, 32783752, 32848848, 32901719, 32943648, 33081951, 33483612, 33497588, 33667027, 33703548, 33716247, 33889378, 33909587, 33997886, 34067064, 34081952, 34092406, 34116446, 34152686, 34232775, 34344242, 34421936, 34443425, 34462777, 34528203, 34528318, 34901002, 34940972, 34964524, 35030394, 35032370, 35038108, 35078192, 35094208, 35114498, 35311457, 35363522, 35411584, 35621092, 35667612, 35754298, 35756691, 35785874, 35796542, 35827462, 35977369, 36009677, 36036599, 36065711, 36096430, 36099505, 36126660, 36214821, 36237124, 36299356, 36371451, 36427681, 36427736, 36553190, 36586268, 36669551, 36995218, 37017585, 37059129, 37077483, 37094656, 37116613, 37177739, 37227817, 37229425, 37243902, 37248683, 37304731, 37439504, 37465026, 37494849, 37779059, 37872858, 37879307, 37893528, 37930336, 38005012, 38162503, 38189371, 38398324, 38473246, 38477093, 38608850, 38638467, 38645012, 38746327, 38860531, 39067131, 39099566, 39188609, 39314422, 39352896, 39381308, 39490647, 39597348, 39730991, 39885538, 39911369, 39920844, 39932636, 39985849, 40018524, 40023789, 40038157, 40059295, 40070047, 40182588, 40233176, 40265042, 40384380, 40486095, 40844714, 40882357, 40934578, 40934807, 41311384, 42410857, 42707590, 44567255, 44933054, 46784685)";
//				
//				//8203606
//				//5766318
//				//16189651
//				
////				int postId = 8203606;
//				
////				String question_query = "SELECT Id FROM filtered_posts WHERE Id = "+postId+";";
//				
//				ResultSet posts = st.executeQuery(question_query);
//				
//				while(posts.next()) {
//					StackOverflowPost post = pl.loadPost(posts.getInt(1));
//					
//					if((int)post.getPostID() == -1 ) {
//						System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " error loading post -> trying to load (" + posts.getInt(1) +") and got ID (" + post.getPostID().intValue() + ")");
//					} else {
//						if(super.showOutput)
//							System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " Pushing post #"+ post.getPostID().intValue() +" to Queue");
//						
//						ObjectMessage msg = super.getSession().createObjectMessage(post);
//						producer.send(msg);
//					}
//				}
//				
//				conn.close();
//				st.close();
//				posts.close();
//
//			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
//				e.printStackTrace();
//			} catch(MySQLNonTransientConnectionException e) {
//				e.printStackTrace();
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		    
//		    producer.close();
//		    
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " Caught: " + e);
		    e.printStackTrace();
		}
	}

	public synchronized void onException(JMSException ex) {
		System.out.println("JMS Exception occured.  Shutting down client.");
	}
}
