package queue;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class QueueStart {
	
	public static void main(String[] args) {
		
		System.out.println("Process startet at " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));

		
		if(args.length > 0) {
			
			int jobID = Integer.parseInt(args[0]);
			
			Thread t = null;
			
			switch(jobID) {
				case 1:
					t = new Thread(new PostObjectLoaderQueue());
					break;
				case 2:
					t = new Thread(new APIDetectorQueue());
					break;
				case 3:
					t = new Thread(new BakerQueryQueue());
					break;
				case 4:
					t = new Thread(new AnalyzeAcceptedAnswerQueue());
					break;
				case 5:
					t = new Thread(new AcceptedAnsBakerQueryQueue());
					break;
				case 6:
					t = new Thread(new PostStoreQueue());
					break;
				default:
					System.exit(-1);
					break;
			}
			
			t.start();
			
		}
	}
	
}
