package queue;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;

import worker.DBStore;
import worker.StackOverflowPost;

public class PostStoreQueue extends Queue implements Runnable, ExceptionListener {

	public PostStoreQueue() {
		super();
	}
	
	public void run() {
	
		try {
		    // Create the destination (Topic or Queue)
		    Destination source = super.getSession().createQueue("ProcessedPostsQueue?consumer.prefetchSize=" + super.getPrefetchSize());
		
		    // Create a MessageConsumer from the Session to the Topic or Queue
		    MessageConsumer consumer = super.getSession().createConsumer(source);
		    
		    Message message = null;
            boolean running=true;
            
            DBStore db = new DBStore(super.getMysqlServerUrl(), super.getMysqlDatabaseName(), super.getMysqlUsername(), super.getMysqlPassword(), super.getMysqlDriver());
            
            while(running) {
                message = consumer.receive();
                if (message instanceof ObjectMessage && message != null) {
                    Serializable object = ((ObjectMessage) message).getObject();
                    if (object instanceof StackOverflowPost) {
                    	
                    	StackOverflowPost post = (StackOverflowPost)object;
                    	
                    	if(super.showOutput)
                    		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [PostStore] Storing post #" + post.getPostID() + " to the database");
                    	
                    	System.out.println("min: "+post.getMinVersion()+" | max: "+post.getMaxVersion()+" | foundMinIn: "+post.getFoundMinIn()+" | foundMaxIn: "+post.getFoundMaxIn());
                    	
//                		db.storePost(post);
                		message.acknowledge();
                    	
                    }
                }
            }
		    
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " Caught: " + e);
		    e.printStackTrace();
		}
	}

	public synchronized void onException(JMSException ex) {
		System.out.println("JMS Exception occured.  Shutting down client.");
	}
}