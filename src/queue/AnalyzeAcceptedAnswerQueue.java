package queue;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;

import worker.APIDetector;
import worker.StackOverflowPost;

public class AnalyzeAcceptedAnswerQueue extends Queue implements Runnable, ExceptionListener {

	public AnalyzeAcceptedAnswerQueue() {
		super();
	}
	
	public void run() {
		
		try {
			
		    // Create the destination (Topic or Queue)
		    Destination source = super.getSession().createQueue("BakerQueryQueue?consumer.prefetchSize=" + super.getPrefetchSize());
		    
		    // Create the destination (Topic or Queue)
		    Destination destination = super.getSession().createQueue("ProcessedPostsQueue?consumer.prefetchSize=" + super.getPrefetchSize());

		    // Create a MessageProducer from the Session to the Topic or Queue
		    MessageProducer producer = super.getSession().createProducer(destination);
		    
		 
		    producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		    
		    
		    
		    
			// Create the destination (Topic or Queue)
		    Destination destination_2 = super.getSession().createQueue("AnalyzeAcceptedAnsCodeQueue?consumer.prefetchSize=" + super.getPrefetchSize());

		    // Create a MessageProducer from the Session to the Topic or Queue
		    MessageProducer producer_2 = super.getSession().createProducer(destination_2);
		    
		 
		    producer_2.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		    
		    
		
		    // Create a MessageConsumer from the Session to the Topic or Queue
		    MessageConsumer consumer = super.getSession().createConsumer(source);
		    
		    Message message = null;
            boolean running=true;
            
            while(running) {
                message = consumer.receive();
                if (message instanceof ObjectMessage && message != null) {
                    Serializable object = ((ObjectMessage) message).getObject();
                    if (object instanceof StackOverflowPost) {
                    	
                    	StackOverflowPost post = (StackOverflowPost)object;

                    	if(super.showOutput)
							System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [AnalyzeAcceptedAnswer] Processing post #" + post.getPostID());
                		
                    	try {
                    		new APIDetector(post, 1);
                    		
                    		// if distinct version was found or the accepted answer does not have an accepted answer -> done
                    		if(post.getMinVersion() == post.getMaxVersion() || post.getAcceptedAnswerBody() == null) {
                				message.acknowledge();
                                
                                ObjectMessage msg = super.getSession().createObjectMessage(post);
                                
                                producer.send(msg);
                            } else {
                    			// if there is an accepted answer and it has a code block -> analyze
                    			if(post.getAcceptedAnswerBody() != null && post.getAcceptedAnswerBody().contains("<code>")) {
                    				message.acknowledge();
                                    
                                    ObjectMessage msg = super.getSession().createObjectMessage(post);
                                    
                                    producer_2.send(msg);
                    			} else {
                    				message.acknowledge();
                                    
                                    ObjectMessage msg = super.getSession().createObjectMessage(post);
                                    
                                    producer.send(msg);
                    			}
                    		}
                            
	                    } catch(Exception e) {
	                		e.printStackTrace();
	                	}
                    	
                    }
                }
            }
		    
		    producer.close();
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
	}

	public synchronized void onException(JMSException ex) {
		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " JMS Exception occured.  Shutting down client.");
	}
}