package queue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;

import worker.BakerQuery;
import worker.StackOverflowPost;

public class BakerQueryQueue extends Queue implements Runnable, ExceptionListener {

	public BakerQueryQueue() {
		super();
	}
	
	public void run() {
		
		
		String html = "<style>table{border-right:1px solid #000;width:100%}tr:nth-child(even){background:#ededed}th{border:1px solid #000;border-right:none;padding:10px 15px;background-color:#fff}td{border-bottom:1px solid #000;border-left:1px solid #000;padding:5px 10px;vertical-align: top;}tr:hover td:first-child{border-left:1pt solid #000}tr:hover td:last-child{border-right:1pt solid #000}tr:hover td{border-top:1pt solid #000;border-bottom:1pt solid #000;background-color:#dbdbdb;}</style>";
				
		html += "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><th>PostID</th><th>Exception</th><th>Tags</th><th>Code</th></tr>\n";
	
		
		
		try {
			
			Random randomGenerator = new Random();
	 		int rndID = randomGenerator.nextInt(100000000);
			

		    // Create the destination (Topic or Queue)
		    Destination source = super.getSession().createQueue("APIDetectorQueue?consumer.prefetchSize=" + super.getPrefetchSize());
		    
		    // Create the destination (Topic or Queue)
		    Destination destination = super.getSession().createQueue("BakerQueryQueue?consumer.prefetchSize=" + super.getPrefetchSize());

		    // Create a MessageProducer from the Session to the Topic or Queue
		    MessageProducer producer = super.getSession().createProducer(destination);
		    
		    // Create the destination (Topic or Queue)
		    Destination finishedDestionation = super.getSession().createQueue("ProcessedPostsQueue?consumer.prefetchSize=" + super.getPrefetchSize());
		    
		    // Create a MessageProducer from the Session to the Topic or Queue
		    MessageProducer producerFinishedPost = super.getSession().createProducer(finishedDestionation);
		    
		 
		    producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		    
		    producerFinishedPost.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		
		    // Create a MessageConsumer from the Session to the Topic or Queue
		    MessageConsumer consumer = super.getSession().createConsumer(source);
		    
		    Message message = null;
            boolean running=true;
            
            ArrayList<Integer> ids = new ArrayList<Integer>();
            
            int i = 0;
            while(running) {
                message = consumer.receive();
                if (message instanceof ObjectMessage && message != null) {
                    Serializable object = ((ObjectMessage) message).getObject();
                    if (object instanceof StackOverflowPost) {
                    	
                    	i++;
                    	StackOverflowPost post = (StackOverflowPost)object;
                    	
                    	if(super.showOutput)
							System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [BakerQuery] Processing post #" + post.getPostID());
                    	
                    	try {
                    		
                    		long start = System.currentTimeMillis();
                    		BakerQuery bq = new BakerQuery(post);
                    		long duration = System.currentTimeMillis() - start;
	                        
                    		
                    		if(super.showOutput)
    							System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [BakerQuery] " + post.getMinVersion() + " - " + post.getMaxVersion() + " :: DURATION " + duration + "ms");
	                        
	                        ObjectMessage msg = super.getSession().createObjectMessage(post);
	                        
	                        System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [BakerQuery] " + post.getFoundMinIn() + " / " + post.getFoundMaxIn());
                        	
	                        
	                        // if a distinct version was found
	                        if(post.getMinVersion() == post.getMaxVersion()) {
	                        	// send it to the last queue
	                        	if(super.showOutput)
	    							System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [BakerQuery] " + post.getMinVersion() + " == " + post.getMaxVersion() + " --> sending Post to ProcessedPostsQueue");
	                        	message.acknowledge();
	                        	producerFinishedPost.send(msg);
	                        } else {
	                        	// send it to the next queue
	                        	if(super.showOutput)
	    							System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [BakerQuery] " + post.getMinVersion() + " != " + post.getMaxVersion() + " --> sending Post to BakerQueryQueue");
	                        	message.acknowledge();
	                        	producer.send(msg);
	                        }
	                        
	                        
	                        
	                        if(super.showOutput) {
								System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [BakerQuery] Processed post #" + post.getPostID() + "[ "+ i +" ]");
								System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " -----------------------------------------------------------");
	                        }
                    	} catch(com.sun.jersey.api.client.ClientHandlerException e) {
                    		
                    		// neo4j crashed
                    		e.printStackTrace();
                    		
                    		BufferedWriter bw = null;
                    		FileWriter fw = null;

                    		try {

                    			File file = new File("failedIDs_"+rndID+".txt");

                    			// if file doesnt exists, then create it
                    			if (!file.exists()) {
                    				file.createNewFile();
                    			}

                    			// true = append file
                    			fw = new FileWriter(file.getAbsoluteFile(), true);
                    			bw = new BufferedWriter(fw);

                    			bw.write(post.getPostID().toString() + "\n");

                    		} catch (IOException e2) {
                    			e2.printStackTrace();
                    		} finally {
                    			try {
                    				if (bw != null)
                    					bw.close();

                    				if (fw != null)
                    					fw.close();

                    			} catch (IOException ex) {
                    				ex.printStackTrace();
                    			}
                    		}
                    		
                    		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [BakerQuery] EXCEPTION THROWN - NEO4J IS DEAD");
                    		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " -----------------------------------------------------------");
                    		
                    	} catch(Exception e) {
                    		
                    		BufferedWriter bw = null;
                    		FileWriter fw = null;

                    		try {

                    			File file = new File("text.html");

                    			// if file doesnt exists, then create it
                    			if (!file.exists()) {
                    				file.createNewFile();
                    			}

                    			// true = append file
                    			fw = new FileWriter(file.getAbsoluteFile(), true);
                    			bw = new BufferedWriter(fw);

                    			bw.write(html + "<tr><td><a href=\"https://stackoverflow.com/questions/"+post.getPostID().toString()+"\">"+post.getPostID().toString()+"</a></td><td>"+e.getClass().getCanonicalName()+"</td><td>"+post.getTags().toString().replace("<","&lt;").replace(">","&gt;")+"</td><td>"+extractCodeFromPost(post.getBody())+"</tr>\n");

                    			html = "";
                    			
                    		} catch (IOException e2) {
                    			e2.printStackTrace();
                    		} finally {
                    			try {
                    				if (bw != null)
                    					bw.close();

                    				if (fw != null)
                    					fw.close();

                    			} catch (IOException ex) {
                    				ex.printStackTrace();
                    			}
                    		
                    		}
                    		
                    		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " [BakerQuery] EXCEPTION THROWN [ "+ i +" ]");
                    		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " -----------------------------------------------------------");
                    		
	                    	e.printStackTrace();
	                	}
                    }
                }
            }
		    
            producerFinishedPost.close();
		    producer.close();
		    
		} catch (Exception e) {
		    System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " Caught: " + e);
		    e.printStackTrace();
		}
	}
	
	public String extractCodeFromPost(String body) {

		String codeOfBody = "";

		Pattern pattern = Pattern.compile("<code>(.+?)</code>");
		Matcher matcher = pattern.matcher(body);
		
		//Pattern logcatRegex = Pattern.compile("^([0-9-]{5}[ ][0-9:.]{13}[ ][A-Z/]{2})");
		
		
		while (matcher.find()) {
			
			//

			String code = matcher.group(1);
			if (code.contains(" ") || code.contains("\\.")) {
				
				// split at line breaks
				String[] lines = code.split("&#xA;");
				
				// check each line, if it is a logcat entry (they cause exception in the baker-tool
				for(String s : lines) {
					//Matcher logcatMatcher = logcatRegex.matcher(s.trim());
					
					//if(!logcatMatcher.find()){
						codeOfBody += s + "&#xA;";
					//}
				}
				
			}

		}
		
		return codeOfBody.replace(" ", "&nbsp;").replace("<","&lt;").replace(">","&gt;").replaceAll("&#xA;", "<br />");
	}

	public synchronized void onException(JMSException ex) {
		System.out.println(new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(new Date()) + " JMS Exception occured.  Shutting down client.");
	}
}